﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VodkaDelivery.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<VodkaDelivery.Data.Vodka, VodkaDelivery.Web.Models.Vodka>().
                ForMember(dest => dest.ID, map => map.MapFrom(src => src.Vodka_ID)).
                ForMember(dest => dest.Company_ID, map => map.MapFrom(src => src.Company_ID)).
                ForMember(dest => dest.VodkaName, map => map.MapFrom(src => src.Vodka_Name)).
                ForMember(dest => dest.NumberOfDestills, map => map.MapFrom(src => src.Number_of_Destills)).
                ForMember(dest => dest.Price, map => map.MapFrom(src => src.Price)).
                ForMember(dest => dest.Volume, map => map.MapFrom(src => src.Volume)).
                ForMember(dest => dest.Flavoured, map => map.MapFrom(src => src.Flavoured)).
                ForMember(dest => dest.Material, map => map.MapFrom(src => src.Material)).
                ForMember(dest => dest.Qualification, map => map.MapFrom(src => src.Qualification));
            });
            return config.CreateMapper();
        }

        public static IMapper ReverseMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<VodkaDelivery.Web.Models.Vodka, VodkaDelivery.Data.Vodka>().
                ForMember(dest => dest.Vodka_ID, map => map.MapFrom(src => src.ID)).
                ForMember(dest => dest.Company_ID, map => map.MapFrom(src => src.Company_ID)).
                ForMember(dest => dest.Vodka_Name, map => map.MapFrom(src => src.VodkaName)).
                ForMember(dest => dest.Number_of_Destills, map => map.MapFrom(src => src.NumberOfDestills)).
                ForMember(dest => dest.Price, map => map.MapFrom(src => src.Price)).
                ForMember(dest => dest.Volume, map => map.MapFrom(src => src.Volume)).
                ForMember(dest => dest.Flavoured, map => map.MapFrom(src => src.Flavoured)).
                ForMember(dest => dest.Material, map => map.MapFrom(src => src.Material)).
                ForMember(dest => dest.Qualification, map => map.MapFrom(src => src.Qualification));
            });
            return config.CreateMapper();
        }
    }
}