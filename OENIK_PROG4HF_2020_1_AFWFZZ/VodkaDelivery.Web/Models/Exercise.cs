﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFWFZZ_HW.Models
{
    public class Exercise
    {
        public string Name { get; set; }
        public double BurnCaloriesInOneHour { get; set; }

        public Exercise(string Name, double BurnCaloriesInOneHour)
        {
            this.Name = Name;
            this.BurnCaloriesInOneHour = BurnCaloriesInOneHour;
        }
    }
}