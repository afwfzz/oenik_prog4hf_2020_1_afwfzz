﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VodkaDelivery.Web.Models
{
    //Form model
    public class Vodka
    {
        [Display( Name = "Vodka ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Company ID")]
        [Required]
        public int Company_ID { get; set; }

        [Display(Name = "Vodka Name")]
        [Required]
        public string VodkaName { get; set; }

        [Display(Name = "Number Of Destills")]
        [Required]
        public int NumberOfDestills { get; set; }

        [Display(Name = "Vodka Price")]
        [Required]
        public int Price { get; set; }

        [Display(Name = "Vodka Volume")]
        [Required]
        public float Volume { get; set; }

        [Display(Name = "Vodka Flavoured")]
        [Required]
        public string Flavoured { get; set; }

        [Display(Name = "Vodka Material")]
        [Required]
        public string Material { get; set; }

        [Display(Name = "Vodka Qualification")]
        [Required]
        public int Qualification { get; set; }
    }
}