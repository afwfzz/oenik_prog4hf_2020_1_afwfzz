﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VodkaDelivery.Web.Models
{
    public class VodkasViewModel
    {
        public Vodka EditedVodka { get; set; }
        public List<Vodka> ListOfVodkas { get; set; }
    }
}