﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFWFZZ_HW.Models
{
    public class Person
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public string Gyakorlat { get; set; }
        public Exercise Exercise { get; set; }
        public double LengthOfExercise { get; set; }
        public double BurntCalories { get; set; }
    }
}