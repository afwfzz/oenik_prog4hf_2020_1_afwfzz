﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VodkaDelivery.Data;
using VodkaDelivery.Logic;
using VodkaDelivery.Repository;
using VodkaDelivery.Web.Models;

namespace VodkaDelivery.Web.Controllers
{
    public class VodkaController : Controller
    {
        IVodkaLogic logic;
        IMapper mapper;
        IMapper reverse;
        VodkasViewModel vm;

        public VodkaController()
        {
            vodkadeliverydatabaseEntities entities = new vodkadeliverydatabaseEntities();
            VodkaRepository vodkaRepo = new VodkaRepository(entities);
            logic = new VodkaLogic(vodkaRepo);
            mapper = MapperFactory.CreateMapper();
            reverse = MapperFactory.ReverseMapper();
            vm = new VodkasViewModel();
            vm.EditedVodka = new Models.Vodka();
            var vodkas = logic.GetVodkas();
            vm.ListOfVodkas = mapper.Map<IList<Data.Vodka>, List<Models.Vodka>>(vodkas);
        }

        private Models.Vodka GetVodkaModel(int id)
        {
            Data.Vodka oneVodka = logic.GetOne(id);
            return mapper.Map<Data.Vodka, Models.Vodka>(oneVodka);
        }

        private Data.Vodka GetVodkaData(Models.Vodka vodka)
        {
            return reverse.Map<Models.Vodka, Data.Vodka>(vodka);
        }

        private List<string> EditedVodkaDatas(Models.Vodka vodka)
        {
            List<string> toBack = new List<string>();
            toBack.Add(vodka.ID.ToString());
            toBack.Add(vodka.Company_ID.ToString());
            toBack.Add(vodka.VodkaName.ToString());
            toBack.Add(vodka.NumberOfDestills.ToString());
            toBack.Add(vodka.Price.ToString());
            toBack.Add(vodka.Volume.ToString());
            toBack.Add(vodka.Flavoured.ToString());
            toBack.Add(vodka.Material.ToString());
            toBack.Add(vodka.Qualification.ToString());
            return toBack;
        }

        // GET: Vodka
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("VodkaIndex", vm);
        }

        // GET: Vodka/Details/5
        public ActionResult Details(int id)
        {
            return View("VodkaDetails", GetVodkaModel(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (logic.DeleteVodka(id)) TempData["editResult"] = "Delete OK";
            return RedirectToAction(nameof(Index));
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedVodka = GetVodkaModel(id);
            return View("VodkaIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Models.Vodka vodka, string editAction)
        {
            if(ModelState.IsValid && vodka != null)
            {
                TempData["editResult"] = "Edit OK";
                if(editAction == "AddNew")
                {
                    logic.InsertIntoVodka(GetVodkaData(vodka));
                }
                else
                {
                    bool success = logic.UpdateVodka(vodka.ID, EditedVodkaDatas(vodka));
                    if(!success) TempData["editResult"] = "Edit FAIL";
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedVodka = vodka;
                return View("VodkaIndex", vm);
            }
        }
    }
}
