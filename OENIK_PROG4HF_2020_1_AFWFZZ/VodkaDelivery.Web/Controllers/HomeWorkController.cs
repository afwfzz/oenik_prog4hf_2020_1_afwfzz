﻿using AFWFZZ_HW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AFWFZZ_HW.Controllers
{
    public class HomeWorkController : Controller
    {
        private List<Exercise> GetAList()
        {
            List<Exercise> exercises = new List<Exercise>();
            exercises.Add(new Exercise("Running", 1000));
            exercises.Add(new Exercise("Yoga", 400));
            exercises.Add(new Exercise("Pilates", 472));
            exercises.Add(new Exercise("Hiking", 700));
            exercises.Add(new Exercise("Swimming", 1000));
            exercises.Add(new Exercise("Bicycle", 600));
            return exercises;
        }

        public ActionResult Calorie()
        {
            return View("CalorieInput");
        }

        // POST: HomeWork/Calorie
        [HttpPost]
        public ActionResult Calorie(Person person)
        {
            List<Exercise> exercises = GetAList();

            int i = 0;

            while (i <exercises.Count)
            {
                if (exercises[i].Name == person.Gyakorlat)
                {
                    person.Exercise = new Exercise(exercises[i].Name,exercises[i].BurnCaloriesInOneHour);
                    i = exercises.Count + 1;
                }
                i++;
            }

            person.BurntCalories = (person.LengthOfExercise / 60) * person.Exercise.BurnCaloriesInOneHour;
            return View("CalorieResult",person);
        }
    }
}