﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VodkaDelivery.Data;
using VodkaDelivery.Logic;
using VodkaDelivery.Repository;
using VodkaDelivery.Web.Models;

namespace VodkaDelivery.Web.Controllers
{
    public class VodkaApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        IVodkaLogic logic;
        IMapper mapper;
        IMapper reverse;

        public VodkaApiController()
        {
            vodkadeliverydatabaseEntities entities = new vodkadeliverydatabaseEntities();
            VodkaRepository vodkaRepo = new VodkaRepository(entities);
            logic = new VodkaLogic(vodkaRepo);
            mapper = MapperFactory.CreateMapper();
            reverse = MapperFactory.ReverseMapper();
        }

        private List<string> EditedVodkaDatas(Models.Vodka vodka)
        {
            List<string> toBack = new List<string>();
            toBack.Add(vodka.ID.ToString());
            toBack.Add(vodka.Company_ID.ToString());
            toBack.Add(vodka.VodkaName.ToString());
            toBack.Add(vodka.NumberOfDestills.ToString());
            toBack.Add(vodka.Price.ToString());
            toBack.Add(vodka.Volume.ToString());
            toBack.Add(vodka.Flavoured.ToString());
            toBack.Add(vodka.Material.ToString());
            toBack.Add(vodka.Qualification.ToString());
            return toBack;
        }

        private Data.Vodka GetVodkaData(Models.Vodka vodka)
        {
            return reverse.Map<Models.Vodka, Data.Vodka>(vodka);
        }

        //GET api/VodkaApi/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Vodka> GetAll()
        {
            var vodkas = logic.GetVodkas();
            return mapper.Map<IList<Data.Vodka>, List<Models.Vodka>>(vodkas);
        }

        //GET api/VodkaApi/del/id
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneVodka(int id)
        {
            bool succes = logic.DeleteVodka(id);
            return new ApiResult() { OperationResult = succes };
        }

        //POST api/VodkaApi/add + vodka
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneVodka(Models.Vodka vodka)
        {
            logic.InsertIntoVodka(GetVodkaData(vodka));
            return new ApiResult() { OperationResult = true };
        }

        //POST api/VodkaApi/mod + vodka
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneCar(Models.Vodka vodka)
        {
            bool succes = logic.UpdateVodka(vodka.ID, EditedVodkaDatas(vodka));
            return new ApiResult() { OperationResult = succes };
        }
    }
}
