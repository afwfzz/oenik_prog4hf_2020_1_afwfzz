﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace VodkaDelivery.ConsoleClient
{
    public class Vodka
    {
        public int ID { get; set; }
        
        public int Company_ID { get; set; }
        
        public string VodkaName { get; set; }
        
        public int NumberOfDestills { get; set; }
        
        public int Price { get; set; }
        
        public float Volume { get; set; }
        
        public string Flavoured { get; set; }
        
        public string Material { get; set; }
        
        public int Qualification { get; set; }

        public override string ToString()
        {
            return $"ID:{ID}\tCompany_ID:{Company_ID}\tVodka Name:{VodkaName}\tNumber of Destills:{NumberOfDestills}\tPrice:{Price}\t" +
                $"Volume:{Volume}\tFlavoured:{Flavoured}\tMaterial:{Material}\tQualification:{Qualification}\t";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            string url = "http://localhost:49389/api/VodkaApi/";
            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Vodka>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();
                Console.WriteLine("------------------------------------ADD---------------------------------------------");

                Dictionary<string, string> postDatas;
                string response;

                postDatas = new Dictionary<string, string>();
                postDatas.Add(nameof(Vodka.Company_ID), "2");
                postDatas.Add(nameof(Vodka.VodkaName), "TestVodka");
                postDatas.Add(nameof(Vodka.NumberOfDestills), "3");
                postDatas.Add(nameof(Vodka.Price), "9999");
                postDatas.Add(nameof(Vodka.Volume), "40");
                postDatas.Add(nameof(Vodka.Flavoured), "Blueberry");
                postDatas.Add(nameof(Vodka.Material), "wheat");
                postDatas.Add(nameof(Vodka.Qualification), "10");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postDatas)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                Console.WriteLine("------------------------------------MOD---------------------------------------------");
                int vodkaID = JsonConvert.DeserializeObject<List<Vodka>>(json).Single(x => x.VodkaName == "TestVodka").ID;
                postDatas = new Dictionary<string, string>();
                postDatas.Add(nameof(Vodka.ID), vodkaID.ToString());
                postDatas.Add(nameof(Vodka.Company_ID), "2");
                postDatas.Add(nameof(Vodka.VodkaName), "TestVodka");
                postDatas.Add(nameof(Vodka.NumberOfDestills), "3");
                postDatas.Add(nameof(Vodka.Price), "8888888");
                postDatas.Add(nameof(Vodka.Volume), "40");
                postDatas.Add(nameof(Vodka.Flavoured), "Blueberry");
                postDatas.Add(nameof(Vodka.Material), "wheat");
                postDatas.Add(nameof(Vodka.Qualification), "10");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postDatas)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
                Console.WriteLine("------------------------------------DEL---------------------------------------------");
                response = client.GetStringAsync(url + "del/"+vodkaID).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }
        }
    }
}
