var interface_vodka_delivery_1_1_logic_1_1_i_vodka_logic =
[
    [ "DeleteVodka", "interface_vodka_delivery_1_1_logic_1_1_i_vodka_logic.html#a51536674b2524c28b49c5d5b6f19e23f", null ],
    [ "GetCompanyAndVodkaDatas", "interface_vodka_delivery_1_1_logic_1_1_i_vodka_logic.html#ac863c78ff36612d80fb60ba3aaa212ec", null ],
    [ "GetVodkas", "interface_vodka_delivery_1_1_logic_1_1_i_vodka_logic.html#ab600cc09c2251834daf8fe8c3d1f40b4", null ],
    [ "InsertIntoVodka", "interface_vodka_delivery_1_1_logic_1_1_i_vodka_logic.html#a41e089d1ed0342ed3ce4d41c12b5dc31", null ],
    [ "UpdateVodka", "interface_vodka_delivery_1_1_logic_1_1_i_vodka_logic.html#acb7f206a93108e90ee71fb8ad2e19031", null ]
];