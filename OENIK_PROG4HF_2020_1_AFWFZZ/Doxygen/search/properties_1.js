var searchData=
[
  ['ceo_187',['CEO',['../class_vodka_delivery_1_1_data_1_1_company.html#a52452a28ec2279ecbf2fbb67c3ffc73f',1,'VodkaDelivery.Data.Company.CEO()'],['../class_vodka_delivery_1_1_data_1_1_shop.html#ac3e13959c77bfa9a7e27a51e088d5839',1,'VodkaDelivery.Data.Shop.CEO()']]],
  ['client_188',['Client',['../class_vodka_delivery_1_1_data_1_1_delivery.html#a376865fbbc42d6996595f4654843f74e',1,'VodkaDelivery::Data::Delivery']]],
  ['company_189',['Company',['../class_vodka_delivery_1_1_data_1_1_vodka.html#a38a95053c6298747969a9c010f6e1736',1,'VodkaDelivery::Data::Vodka']]],
  ['company_5fid_190',['Company_ID',['../class_vodka_delivery_1_1_data_1_1_company.html#a35f0d82c9aa8eb529e060b9e23a78bff',1,'VodkaDelivery.Data.Company.Company_ID()'],['../class_vodka_delivery_1_1_data_1_1_vodka.html#ac00fe18b8168318e2f8042830793578e',1,'VodkaDelivery.Data.Vodka.Company_ID()']]],
  ['country_191',['Country',['../class_vodka_delivery_1_1_data_1_1_company.html#a7f8e68278fd574466178dd8eebb308c6',1,'VodkaDelivery::Data::Company']]]
];
