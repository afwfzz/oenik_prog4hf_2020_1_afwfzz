var searchData=
[
  ['setup_60',['SetUp',['../class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#abd66c839e89e9fe4de5642cdfcc94ae7',1,'VodkaDelivery::Logic::Tests::Tests']]],
  ['shop_61',['Shop',['../class_vodka_delivery_1_1_data_1_1_shop.html',1,'VodkaDelivery.Data.Shop'],['../class_vodka_delivery_1_1_data_1_1_delivery.html#af1d38d19a5ea33d62db5887f47ae1e95',1,'VodkaDelivery.Data.Delivery.Shop()'],['../class_vodka_delivery_1_1_data_1_1_shop.html#a23b2be48e553288f80d1cec07ddbe107',1,'VodkaDelivery.Data.Shop.Shop()']]],
  ['shop_5fid_62',['Shop_ID',['../class_vodka_delivery_1_1_data_1_1_delivery.html#ab83017ac42ad5d4f915521d7f98d4cb5',1,'VodkaDelivery.Data.Delivery.Shop_ID()'],['../class_vodka_delivery_1_1_data_1_1_shop.html#ac1ee512b423f21f88446c8c6796057cc',1,'VodkaDelivery.Data.Shop.Shop_ID()']]],
  ['shoplogic_63',['ShopLogic',['../class_vodka_delivery_1_1_logic_1_1_shop_logic.html',1,'VodkaDelivery.Logic.ShopLogic'],['../class_vodka_delivery_1_1_logic_1_1_shop_logic.html#ae3cce9e20f870f13bae6042a307e416d',1,'VodkaDelivery.Logic.ShopLogic.ShopLogic()']]],
  ['shoprepository_64',['ShopRepository',['../class_vodka_delivery_1_1_repository_1_1_shop_repository.html',1,'VodkaDelivery::Repository']]],
  ['splitxml_65',['SplitXml',['../class_vodka_delivery_1_1_logic_1_1_end_point_logic.html#ab8d9a977f46d456619d7150f0cf8f9df',1,'VodkaDelivery::Logic::EndPointLogic']]],
  ['status_66',['Status',['../class_vodka_delivery_1_1_data_1_1_delivery.html#ad5089c3ab6828a4558ce5f08b8219463',1,'VodkaDelivery::Data::Delivery']]]
];
