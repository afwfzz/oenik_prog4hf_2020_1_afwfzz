var indexSectionsWithContent =
{
  0: "acdefgimnpqstuvx",
  1: "cdeistv",
  2: "v",
  3: "cdgistuvx",
  4: "acdefmnpqsv",
  5: "cn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "properties",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Properties",
  5: "Pages"
};

