var class_vodka_delivery_1_1_logic_1_1_delivery_logic =
[
    [ "DeliveryLogic", "class_vodka_delivery_1_1_logic_1_1_delivery_logic.html#a383889294a796bdd27434d2619c5c33c", null ],
    [ "DeteleDelivery", "class_vodka_delivery_1_1_logic_1_1_delivery_logic.html#a72a831e60124c358ef4ef579570a89cb", null ],
    [ "GetDeliveries", "class_vodka_delivery_1_1_logic_1_1_delivery_logic.html#a89ca07487ff96ab5db6683ed6e4b834e", null ],
    [ "GetDeliveriesUsingVodkaName", "class_vodka_delivery_1_1_logic_1_1_delivery_logic.html#a67f6c989a5ca0293af3ab88ff5af7dc1", null ],
    [ "InsertIntoDelivery", "class_vodka_delivery_1_1_logic_1_1_delivery_logic.html#ae6b589a5896ce6cc60bfc9778e0736f1", null ],
    [ "UpdateDelivery", "class_vodka_delivery_1_1_logic_1_1_delivery_logic.html#a06fdc482a7f5e4e2d9707836deedb4cd", null ]
];