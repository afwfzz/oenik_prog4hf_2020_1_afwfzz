var class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests =
[
    [ "GetCompanyAndVodkaDatas", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#aba3e5ff8b71202579fdbc3e80f859577", null ],
    [ "GetDeliveriesUsingVodkaName", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a8671db0174b3337941a25ed8504ceb20", null ],
    [ "GetExpensiveVodkas", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#aba4224ce407dabcc046d15902b7c9aa1", null ],
    [ "SetUp", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#abd66c839e89e9fe4de5642cdfcc94ae7", null ],
    [ "TestDeleteCompany", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a8f2ed3546b042d3651577c0a63d6fa24", null ],
    [ "TestDeleteDelivery", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a419b2ec71e313e94190fa2db8fcbdea3", null ],
    [ "TestDeleteShop", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a91c8152afb00a3c4d6a2dbcf813e1a62", null ],
    [ "TestDeleteVodka", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a217e830fe1ab4d5d268075dc8fc9ef4d", null ],
    [ "TestInsertCompany", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#ab1519b74f34f572b537cc6b6ccf6306b", null ],
    [ "TestInsertDelivery", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a8a161e798403a3f3ef554cec562c2c62", null ],
    [ "TestInsertShop", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a4ed8ef3a8a7bbdb0e8c130540450e983", null ],
    [ "TestInsertVodka", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#ac20a8e6e83621a06533d5cd715a9a72f", null ],
    [ "TestReadCompanies", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a61bcd59eadcb6f0e7d5e815d50c8461b", null ],
    [ "TestReadDeliveries", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#aeeea570cd2cbd23d45e030143e72161b", null ],
    [ "TestReadShops", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#ab455a3625452a6b4de19cfc22682b84b", null ],
    [ "TestReadVodkas", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a79e5002cac36b9ab450fdde522816014", null ],
    [ "TestUpdateCompany", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a65b06a6b9f4acdc8bcc1249c7d5fb9b6", null ],
    [ "TestUpdateDelivery", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a0b9572e603b2082d7a049513721076a6", null ],
    [ "TestUpdateShop", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a148cb1c47620b509fe6b6b2f3f03a49d", null ],
    [ "TestUpdateVodka", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html#a32945c2f58a63384f646aca69b113f4a", null ]
];