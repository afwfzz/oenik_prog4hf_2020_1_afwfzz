var namespace_vodka_delivery_1_1_repository =
[
    [ "CompanyRepository", "class_vodka_delivery_1_1_repository_1_1_company_repository.html", "class_vodka_delivery_1_1_repository_1_1_company_repository" ],
    [ "DeliveryRepository", "class_vodka_delivery_1_1_repository_1_1_delivery_repository.html", "class_vodka_delivery_1_1_repository_1_1_delivery_repository" ],
    [ "ICompanyRepository", "interface_vodka_delivery_1_1_repository_1_1_i_company_repository.html", "interface_vodka_delivery_1_1_repository_1_1_i_company_repository" ],
    [ "IDeliveryRepository", "interface_vodka_delivery_1_1_repository_1_1_i_delivery_repository.html", "interface_vodka_delivery_1_1_repository_1_1_i_delivery_repository" ],
    [ "IRepository", "interface_vodka_delivery_1_1_repository_1_1_i_repository.html", "interface_vodka_delivery_1_1_repository_1_1_i_repository" ],
    [ "IShopRepository", "interface_vodka_delivery_1_1_repository_1_1_i_shop_repository.html", null ],
    [ "IVodkaRepository", "interface_vodka_delivery_1_1_repository_1_1_i_vodka_repository.html", "interface_vodka_delivery_1_1_repository_1_1_i_vodka_repository" ],
    [ "ShopRepository", "class_vodka_delivery_1_1_repository_1_1_shop_repository.html", "class_vodka_delivery_1_1_repository_1_1_shop_repository" ],
    [ "VodkaRepository", "class_vodka_delivery_1_1_repository_1_1_vodka_repository.html", "class_vodka_delivery_1_1_repository_1_1_vodka_repository" ]
];