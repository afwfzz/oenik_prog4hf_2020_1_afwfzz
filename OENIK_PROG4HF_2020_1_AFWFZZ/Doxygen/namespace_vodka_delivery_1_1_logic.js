var namespace_vodka_delivery_1_1_logic =
[
    [ "Tests", "namespace_vodka_delivery_1_1_logic_1_1_tests.html", "namespace_vodka_delivery_1_1_logic_1_1_tests" ],
    [ "CompanyLogic", "class_vodka_delivery_1_1_logic_1_1_company_logic.html", "class_vodka_delivery_1_1_logic_1_1_company_logic" ],
    [ "DeliveryLogic", "class_vodka_delivery_1_1_logic_1_1_delivery_logic.html", "class_vodka_delivery_1_1_logic_1_1_delivery_logic" ],
    [ "EndPointLogic", "class_vodka_delivery_1_1_logic_1_1_end_point_logic.html", "class_vodka_delivery_1_1_logic_1_1_end_point_logic" ],
    [ "ICompanyLogic", "interface_vodka_delivery_1_1_logic_1_1_i_company_logic.html", "interface_vodka_delivery_1_1_logic_1_1_i_company_logic" ],
    [ "IDeliveryLogic", "interface_vodka_delivery_1_1_logic_1_1_i_delivery_logic.html", "interface_vodka_delivery_1_1_logic_1_1_i_delivery_logic" ],
    [ "IShopLogic", "interface_vodka_delivery_1_1_logic_1_1_i_shop_logic.html", "interface_vodka_delivery_1_1_logic_1_1_i_shop_logic" ],
    [ "IVodkaLogic", "interface_vodka_delivery_1_1_logic_1_1_i_vodka_logic.html", "interface_vodka_delivery_1_1_logic_1_1_i_vodka_logic" ],
    [ "ShopLogic", "class_vodka_delivery_1_1_logic_1_1_shop_logic.html", "class_vodka_delivery_1_1_logic_1_1_shop_logic" ],
    [ "VodkaLogic", "class_vodka_delivery_1_1_logic_1_1_vodka_logic.html", "class_vodka_delivery_1_1_logic_1_1_vodka_logic" ]
];