var class_vodka_delivery_1_1_repository_1_1_delivery_repository =
[
    [ "Delete", "class_vodka_delivery_1_1_repository_1_1_delivery_repository.html#afa8e3b3c10890d83ab1ee60c4563a3ea", null ],
    [ "GetAll", "class_vodka_delivery_1_1_repository_1_1_delivery_repository.html#adf0ac56dfa88cd3d8570031636f88816", null ],
    [ "GetDeliveriesUsingVodkaName", "class_vodka_delivery_1_1_repository_1_1_delivery_repository.html#a7865eccfdbfb51e638428e5f88d4da8f", null ],
    [ "InsertIn", "class_vodka_delivery_1_1_repository_1_1_delivery_repository.html#a7b5a72dad0fda428862a5db6090d2b84", null ],
    [ "IsIn", "class_vodka_delivery_1_1_repository_1_1_delivery_repository.html#a1f061cc0728e2e511e19f669198ba7aa", null ],
    [ "Update", "class_vodka_delivery_1_1_repository_1_1_delivery_repository.html#a25cb1d2653d24c072074655e3b60fa60", null ]
];