var class_vodka_delivery_1_1_logic_1_1_vodka_logic =
[
    [ "VodkaLogic", "class_vodka_delivery_1_1_logic_1_1_vodka_logic.html#a4aa724f2d92c8138bf4edcd7ee4a6034", null ],
    [ "DeleteVodka", "class_vodka_delivery_1_1_logic_1_1_vodka_logic.html#a8e84341835c7dfdfa1efc0653a9a4741", null ],
    [ "GetCompanyAndVodkaDatas", "class_vodka_delivery_1_1_logic_1_1_vodka_logic.html#a882d483cdd055aaca389c8afdb3f1c42", null ],
    [ "GetVodkas", "class_vodka_delivery_1_1_logic_1_1_vodka_logic.html#a1716e72adebcff9f4c85cd65cbb1c573", null ],
    [ "InsertIntoVodka", "class_vodka_delivery_1_1_logic_1_1_vodka_logic.html#a5c54f704fd46523eedae16d55c3c286d", null ],
    [ "UpdateVodka", "class_vodka_delivery_1_1_logic_1_1_vodka_logic.html#a688d2719da9368499bd72f2e6db80332", null ]
];