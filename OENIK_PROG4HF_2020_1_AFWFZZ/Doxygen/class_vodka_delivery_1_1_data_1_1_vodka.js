var class_vodka_delivery_1_1_data_1_1_vodka =
[
    [ "Vodka", "class_vodka_delivery_1_1_data_1_1_vodka.html#a33e55e77966f1258b8886814736fd7d0", null ],
    [ "Company", "class_vodka_delivery_1_1_data_1_1_vodka.html#a38a95053c6298747969a9c010f6e1736", null ],
    [ "Company_ID", "class_vodka_delivery_1_1_data_1_1_vodka.html#ac00fe18b8168318e2f8042830793578e", null ],
    [ "Delivery", "class_vodka_delivery_1_1_data_1_1_vodka.html#a1b00e604df1793d8ce22374d5f2b3854", null ],
    [ "Flavoured", "class_vodka_delivery_1_1_data_1_1_vodka.html#aaa892888a1248df70fe4dde2dba3f12d", null ],
    [ "Material", "class_vodka_delivery_1_1_data_1_1_vodka.html#aa325866769f1b80770d49465cb957e01", null ],
    [ "Number_of_Destills", "class_vodka_delivery_1_1_data_1_1_vodka.html#a8e5154ff98de4d1a23a35e3d4bc179fa", null ],
    [ "Price", "class_vodka_delivery_1_1_data_1_1_vodka.html#aa212205590c44259b19da399b0104a60", null ],
    [ "Qualification", "class_vodka_delivery_1_1_data_1_1_vodka.html#a2cbe627997f02fcb1c8e89f9a8e1f28e", null ],
    [ "Vodka_ID", "class_vodka_delivery_1_1_data_1_1_vodka.html#a939ee46f78dcaf1923542feafcd3e1e0", null ],
    [ "Vodka_Name", "class_vodka_delivery_1_1_data_1_1_vodka.html#a7b1d181955ff666a3358bab389014511", null ],
    [ "Volume", "class_vodka_delivery_1_1_data_1_1_vodka.html#aaf110c30da93c0208a50be6b5de245d4", null ]
];