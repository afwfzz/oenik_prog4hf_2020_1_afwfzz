var class_vodka_delivery_1_1_data_1_1_shop =
[
    [ "Shop", "class_vodka_delivery_1_1_data_1_1_shop.html#a23b2be48e553288f80d1cec07ddbe107", null ],
    [ "Address", "class_vodka_delivery_1_1_data_1_1_shop.html#a01ee2d373678ad5557ec86db44a6e4aa", null ],
    [ "CEO", "class_vodka_delivery_1_1_data_1_1_shop.html#ac3e13959c77bfa9a7e27a51e088d5839", null ],
    [ "Delivery", "class_vodka_delivery_1_1_data_1_1_shop.html#a7f6d153a231e7616a9bc0d0daa7a9bc7", null ],
    [ "District", "class_vodka_delivery_1_1_data_1_1_shop.html#ada26c6140d5dbb27e7301d9bccbbbbfc", null ],
    [ "Min_Quantity", "class_vodka_delivery_1_1_data_1_1_shop.html#a98f6166325c37f8d09d9894efe2e72c8", null ],
    [ "Name", "class_vodka_delivery_1_1_data_1_1_shop.html#a3119752c7033b1ee656384482d4f13b3", null ],
    [ "Shop_ID", "class_vodka_delivery_1_1_data_1_1_shop.html#ac1ee512b423f21f88446c8c6796057cc", null ]
];