var interface_vodka_delivery_1_1_logic_1_1_i_delivery_logic =
[
    [ "DeteleDelivery", "interface_vodka_delivery_1_1_logic_1_1_i_delivery_logic.html#a0516a7cdef2393660dca1e59b5cab859", null ],
    [ "GetDeliveries", "interface_vodka_delivery_1_1_logic_1_1_i_delivery_logic.html#a934a3309b5fd3aa3092c18e49c8b90e1", null ],
    [ "GetDeliveriesUsingVodkaName", "interface_vodka_delivery_1_1_logic_1_1_i_delivery_logic.html#a6c165b18707b5f2110c966daeb46d314", null ],
    [ "InsertIntoDelivery", "interface_vodka_delivery_1_1_logic_1_1_i_delivery_logic.html#ab4b8563a39e235a69e04ada0935f9962", null ],
    [ "UpdateDelivery", "interface_vodka_delivery_1_1_logic_1_1_i_delivery_logic.html#a0306a923c8b208d15ea6674b33558c54", null ]
];