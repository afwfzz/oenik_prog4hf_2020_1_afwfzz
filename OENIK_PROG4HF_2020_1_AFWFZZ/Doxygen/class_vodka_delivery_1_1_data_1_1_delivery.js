var class_vodka_delivery_1_1_data_1_1_delivery =
[
    [ "Client", "class_vodka_delivery_1_1_data_1_1_delivery.html#a376865fbbc42d6996595f4654843f74e", null ],
    [ "Date", "class_vodka_delivery_1_1_data_1_1_delivery.html#a78bc35c88b2d2b2f5e637b292bc3d743", null ],
    [ "Deadline", "class_vodka_delivery_1_1_data_1_1_delivery.html#afa0ee0e63e1d8b8aa4fcf531e308935d", null ],
    [ "Delivery_ID", "class_vodka_delivery_1_1_data_1_1_delivery.html#aa91f618cb706377204f3531998fe324b", null ],
    [ "Quantity", "class_vodka_delivery_1_1_data_1_1_delivery.html#ad5b98199fd9479ea1182676757ed1ed6", null ],
    [ "Shop", "class_vodka_delivery_1_1_data_1_1_delivery.html#af1d38d19a5ea33d62db5887f47ae1e95", null ],
    [ "Shop_ID", "class_vodka_delivery_1_1_data_1_1_delivery.html#ab83017ac42ad5d4f915521d7f98d4cb5", null ],
    [ "Status", "class_vodka_delivery_1_1_data_1_1_delivery.html#ad5089c3ab6828a4558ce5f08b8219463", null ],
    [ "Vodka", "class_vodka_delivery_1_1_data_1_1_delivery.html#abff5c51ef17f36f889392433f8cf56f0", null ],
    [ "Vodka_ID", "class_vodka_delivery_1_1_data_1_1_delivery.html#a2cb11ffb3700c2e5cb0ebc53fa9ef8d0", null ]
];