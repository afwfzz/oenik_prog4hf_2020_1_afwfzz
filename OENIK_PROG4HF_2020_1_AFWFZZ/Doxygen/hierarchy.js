var hierarchy =
[
    [ "VodkaDelivery.Data.Company", "class_vodka_delivery_1_1_data_1_1_company.html", null ],
    [ "DbContext", null, [
      [ "VodkaDelivery.Data::vodkadeliverydatabaseEntities", "class_vodka_delivery_1_1_data_1_1vodkadeliverydatabase_entities.html", null ]
    ] ],
    [ "VodkaDelivery.Data.Delivery", "class_vodka_delivery_1_1_data_1_1_delivery.html", null ],
    [ "VodkaDelivery.Logic.EndPointLogic", "class_vodka_delivery_1_1_logic_1_1_end_point_logic.html", null ],
    [ "VodkaDelivery.Logic.ICompanyLogic", "interface_vodka_delivery_1_1_logic_1_1_i_company_logic.html", [
      [ "VodkaDelivery.Logic.CompanyLogic", "class_vodka_delivery_1_1_logic_1_1_company_logic.html", null ]
    ] ],
    [ "VodkaDelivery.Logic.IDeliveryLogic", "interface_vodka_delivery_1_1_logic_1_1_i_delivery_logic.html", [
      [ "VodkaDelivery.Logic.DeliveryLogic", "class_vodka_delivery_1_1_logic_1_1_delivery_logic.html", null ]
    ] ],
    [ "VodkaDelivery.Repository.IRepository< T >", "interface_vodka_delivery_1_1_repository_1_1_i_repository.html", null ],
    [ "VodkaDelivery.Repository.IRepository< Company >", "interface_vodka_delivery_1_1_repository_1_1_i_repository.html", [
      [ "VodkaDelivery.Repository.ICompanyRepository", "interface_vodka_delivery_1_1_repository_1_1_i_company_repository.html", [
        [ "VodkaDelivery.Repository.CompanyRepository", "class_vodka_delivery_1_1_repository_1_1_company_repository.html", null ]
      ] ]
    ] ],
    [ "VodkaDelivery.Repository.IRepository< Delivery >", "interface_vodka_delivery_1_1_repository_1_1_i_repository.html", [
      [ "VodkaDelivery.Repository.IDeliveryRepository", "interface_vodka_delivery_1_1_repository_1_1_i_delivery_repository.html", [
        [ "VodkaDelivery.Repository.DeliveryRepository", "class_vodka_delivery_1_1_repository_1_1_delivery_repository.html", null ]
      ] ]
    ] ],
    [ "VodkaDelivery.Repository.IRepository< Shop >", "interface_vodka_delivery_1_1_repository_1_1_i_repository.html", [
      [ "VodkaDelivery.Repository.IShopRepository", "interface_vodka_delivery_1_1_repository_1_1_i_shop_repository.html", [
        [ "VodkaDelivery.Repository.ShopRepository", "class_vodka_delivery_1_1_repository_1_1_shop_repository.html", null ]
      ] ]
    ] ],
    [ "VodkaDelivery.Repository.IRepository< Vodka >", "interface_vodka_delivery_1_1_repository_1_1_i_repository.html", [
      [ "VodkaDelivery.Repository.IVodkaRepository", "interface_vodka_delivery_1_1_repository_1_1_i_vodka_repository.html", [
        [ "VodkaDelivery.Repository.VodkaRepository", "class_vodka_delivery_1_1_repository_1_1_vodka_repository.html", null ]
      ] ]
    ] ],
    [ "VodkaDelivery.Logic.IShopLogic", "interface_vodka_delivery_1_1_logic_1_1_i_shop_logic.html", [
      [ "VodkaDelivery.Logic.ShopLogic", "class_vodka_delivery_1_1_logic_1_1_shop_logic.html", null ]
    ] ],
    [ "VodkaDelivery.Logic.IVodkaLogic", "interface_vodka_delivery_1_1_logic_1_1_i_vodka_logic.html", [
      [ "VodkaDelivery.Logic.VodkaLogic", "class_vodka_delivery_1_1_logic_1_1_vodka_logic.html", null ]
    ] ],
    [ "VodkaDelivery.Data.Shop", "class_vodka_delivery_1_1_data_1_1_shop.html", null ],
    [ "VodkaDelivery.Logic.Tests.Tests", "class_vodka_delivery_1_1_logic_1_1_tests_1_1_tests.html", null ],
    [ "VodkaDelivery.Data.Vodka", "class_vodka_delivery_1_1_data_1_1_vodka.html", null ]
];