﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;
    using VodkaDelivery.Logic;
    using VodkaDelivery.Repository;

    /// <summary>
    /// Menu system for .Program.
    /// </summary>
    internal class Menu
    {
        private readonly List<string> menuItems = new List<string>()
        {
            "Read",
            "Insert",
            "Delete",
            "Update",
            "Vodka supplies",
            "Company's datas",
            "Expensive vodkas: sorted by companies",
            "Endpoint",
            "Exit",
        };

        private int index = 0;

        /// <summary>
        /// Fully menu system.
        /// </summary>
        public void MenuOut()
        {
            Console.CursorVisible = false;
            vodkadeliverydatabaseEntities entities = new vodkadeliverydatabaseEntities();
            VodkaRepository vodkaRepo = new VodkaRepository(entities);
            CompanyRepository compRepo = new CompanyRepository();
            DeliveryRepository deliRepo = new DeliveryRepository();
            ShopRepository shopRepo = new ShopRepository();
            VodkaLogic vodkaLogic = new VodkaLogic(vodkaRepo);
            CompanyLogic companyLogic = new CompanyLogic(compRepo);
            DeliveryLogic deliveryLogic = new DeliveryLogic(deliRepo);
            ShopLogic shopLogic = new ShopLogic(shopRepo);
            EndPointLogic endPointLogic = new EndPointLogic();
            List<string> tableNames = new List<string>
            {
                "Vodka",
                "Shop",
                "Company",
                "Delivery",
            };

            while (true)
            {
                string selectedItem = this.DrawMenu(this.menuItems);
                if (selectedItem == this.menuItems[0])
                {
                    Console.Clear();
                    string tableName = string.Empty;
                    do
                    {
                        Console.WriteLine("Please chose one table! (Options: Vodka/Company/Delivery/Shop)");
                        tableName = Console.ReadLine();
                    }
                    while (!tableNames.Contains(tableName));
                    Console.Clear();
                    Console.WriteLine($"\nYou have chosen the {tableName} table");
                    if (tableName == "Vodka")
                    {
                        IList<Vodka> vodkas = vodkaLogic.GetVodkas();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"VODKA_ID | COMPANY_ID | VODKA_NAME | NUMBER_OF_DESTILLS | PRICE | VOLUME | FLAVOURED | MATERIAL | QUALIFICATION");
                        Console.ForegroundColor = ConsoleColor.White;
                        foreach (Vodka item in vodkas)
                        {
                            Console.WriteLine($"{item.Vodka_ID}  | {item.Company_ID} | {item.Vodka_Name} | {item.Number_of_Destills} | {item.Price} | {item.Volume} | {item.Flavoured} | {item.Material} | {item.Qualification}");
                        }

                        Console.WriteLine("\nPress any button to return...");
                        Console.ReadLine();
                        Console.Clear();
                    }
                    else if (tableName == "Company")
                    {
                        List<string> companies = companyLogic.GetCompanies();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(companies[0]);
                        Console.ForegroundColor = ConsoleColor.White;
                        for (int i = 1; i < companies.Count; i++)
                        {
                            Console.WriteLine(companies[i]);
                        }

                        Console.WriteLine("\nPress any button to return...");
                        Console.ReadLine();
                        Console.Clear();
                    }
                    else if (tableName == "Delivery")
                    {
                        List<string> deliveries = deliveryLogic.GetDeliveries();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(deliveries[0]);
                        Console.ForegroundColor = ConsoleColor.White;
                        for (int i = 1; i < deliveries.Count; i++)
                        {
                            Console.WriteLine(deliveries[i]);
                        }

                        Console.WriteLine("\nPress any button to return...");
                        Console.ReadLine();
                        Console.Clear();
                    }
                    else if (tableName == "Shop")
                    {
                        List<string> shops = shopLogic.GetShops();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(shops[0]);
                        Console.ForegroundColor = ConsoleColor.White;
                        for (int i = 1; i < shops.Count; i++)
                        {
                            Console.WriteLine(shops[i]);
                        }

                        Console.WriteLine("\nPress any button to return...");
                        Console.ReadLine();
                        Console.Clear();
                    }
                }
                else if (selectedItem == this.menuItems[1])
                {
                    Console.Clear();
                    string tableName = " ";
                    do
                    {
                        Console.WriteLine("Please chose one table! (Options: Vodka/Company/Delivery/Shop)");
                        tableName = Console.ReadLine();
                    }
                    while (!tableNames.Contains(tableName));
                    Console.Clear();
                    Console.WriteLine($"\nYou have chosen the {tableName} table");
                    if (tableName == "Vodka")
                    {
                        Vodka newVodka = new Vodka();
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive the datas of new item!\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        List<string> vodka = new List<string>();
                        Console.Write("ID: ");
                        newVodka.Vodka_ID = int.Parse(Console.ReadLine());
                        Console.Write("Company_ID: ");
                        newVodka.Company_ID = int.Parse(Console.ReadLine());
                        Console.Write("Name of Vodka: ");
                        newVodka.Vodka_Name = Console.ReadLine();
                        Console.Write("Number of Destills: ");
                        newVodka.Number_of_Destills = int.Parse(Console.ReadLine());
                        Console.Write("Price: ");
                        newVodka.Price = int.Parse(Console.ReadLine());
                        Console.Write("Volume: ");
                        newVodka.Volume = double.Parse(Console.ReadLine());
                        Console.Write("Flavoured: ");
                        newVodka.Flavoured = Console.ReadLine();
                        Console.Write("Material: ");
                        newVodka.Material = Console.ReadLine();
                        Console.Write("Qualification: ");
                        newVodka.Qualification = Console.ReadLine();
                        if (vodkaLogic.InsertIntoVodka(newVodka))
                        {
                            Console.WriteLine("\nItem has been inserted!\nPress any button to return...");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                    else if (tableName == "Company")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive the datas of new item!\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        List<string> company = new List<string>();
                        Console.Write("ID: ");
                        company.Add(Console.ReadLine());
                        Console.Write("Name: ");
                        company.Add(Console.ReadLine());
                        Console.Write("Country: ");
                        company.Add(Console.ReadLine());
                        Console.Write("Accessibility: ");
                        company.Add(Console.ReadLine());
                        Console.Write("Estabisment: ");
                        company.Add(Console.ReadLine());
                        Console.Write("CEO: ");
                        company.Add(Console.ReadLine());
                        if (companyLogic.InsertIntoCompany(company))
                        {
                            Console.WriteLine("\nItem has been inserted!\nPress any button to return...");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                    else if (tableName == "Delivery")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive the datas of new item!\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        List<string> delivery = new List<string>();
                        Console.Write("ID: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Vodka_ID: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Shop_ID: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Date: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Status: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Quantity: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Client: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Deadline: ");
                        delivery.Add(Console.ReadLine());
                        if (deliveryLogic.InsertIntoDelivery(delivery))
                        {
                            Console.WriteLine("\nItem has been inserted!\nPress any button to return...");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                    else if (tableName == "Shop")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive the datas of new item!\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        List<string> shop = new List<string>();
                        Console.Write("ID: ");
                        shop.Add(Console.ReadLine());
                        Console.Write("Name: ");
                        shop.Add(Console.ReadLine());
                        Console.Write("CEO: ");
                        shop.Add(Console.ReadLine());
                        Console.Write("Address: ");
                        shop.Add(Console.ReadLine());
                        Console.Write("Min Quantity: ");
                        shop.Add(Console.ReadLine());
                        Console.Write("District: ");
                        shop.Add(Console.ReadLine());
                        if (shopLogic.InsertIntoShop(shop))
                        {
                            Console.WriteLine("\nItem has been inserted!\nPress any button to return...");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                }
                else if (selectedItem == this.menuItems[2])
                {
                    Console.Clear();
                    string tableName = " ";
                    do
                    {
                        Console.WriteLine("Please chose one table! (Options: Vodka/Company/Delivery/Shop)");
                        tableName = Console.ReadLine();
                    }
                    while (!tableNames.Contains(tableName));
                    Console.Clear();
                    Console.WriteLine($"\nYou have chosen the {tableName} table");
                    if (tableName == "Vodka")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive the Vodka_ID that You want to delete!\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("ID :");
                        int id = int.Parse(Console.ReadLine());
                        if (id > 0)
                        {
                            Console.WriteLine("\nLoading...\n" + Environment.NewLine);
                            if (vodkaLogic.DeleteVodka(id) == true)
                            {
                                Console.WriteLine("Item has been successfully deleted!");
                                Console.ReadLine();
                                Console.Clear();
                            }
                            else
                            {
                                Console.WriteLine($"FAILED: Could not delete!(Check again the ID, not valid ID can not be deleted!) {Environment.NewLine}Wrong ID is: {id}{Environment.NewLine}Press any button to continue...");
                                Console.ReadLine();
                                Console.Clear();
                            }
                        }
                        else
                        {
                            Console.WriteLine($"FAILED: ID can not be negative number! {Environment.NewLine}Wrong ID is: {id}{Environment.NewLine}Press any button to continue...");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                    else if (tableName == "Company")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive the Company_ID that You want to delete!\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("ID: ");
                        int id = int.Parse(Console.ReadLine());
                        if (id > 0)
                        {
                            Console.WriteLine("\nLoading...\n" + Environment.NewLine);
                            if (vodkaLogic.DeleteVodka(id) == true)
                            {
                                Console.WriteLine("Item has been successfully deleted!");
                                Console.ReadLine();
                                Console.Clear();
                            }
                            else
                            {
                                Console.WriteLine($"FAILED: Could not delete!(Check again the ID, not valid ID can not be deleted!) {Environment.NewLine}Wrong ID is: {id}{Environment.NewLine}Press any button to continue...");
                                Console.ReadLine();
                                Console.Clear();
                            }
                        }
                        else
                        {
                            Console.WriteLine($"FAILED: ID can not be negative number! {Environment.NewLine}Wrong ID is: {id}{Environment.NewLine}Press any button to continue...");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                    else if (tableName == "Delivery")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive the Delivery_ID that You want to delete!\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("ID: ");
                        int id = int.Parse(Console.ReadLine());
                        if (id > 0)
                        {
                            Console.WriteLine("\nLoading...\n" + Environment.NewLine);
                            if (vodkaLogic.DeleteVodka(id) == true)
                            {
                                Console.WriteLine("Item has been successfully deleted!");
                                Console.ReadLine();
                                Console.Clear();
                            }
                            else
                            {
                                Console.WriteLine($"FAILED: Could not delete!(Check again the ID, not valid ID can not be deleted!) {Environment.NewLine}Wrong ID is: {id}{Environment.NewLine}Press any button to continue...");
                                Console.ReadLine();
                                Console.Clear();
                            }
                        }
                        else
                        {
                            Console.WriteLine($"FAILED: ID can not be negative number! {Environment.NewLine}Wrong ID is: {id}{Environment.NewLine}Press any button to continue...");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                    else if (tableName == "Shop")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive the Shop_ID that You want to delete!\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("ID: ");
                        int id = int.Parse(Console.ReadLine());
                        if (id > 0)
                        {
                            Console.WriteLine("\nLoading...\n" + Environment.NewLine);
                            if (vodkaLogic.DeleteVodka(id) == true)
                            {
                                Console.WriteLine("Item has been successfully deleted!");
                                Console.ReadLine();
                                Console.Clear();
                            }
                            else
                            {
                                Console.WriteLine($"FAILED: Could not delete!(Check again the ID, not valid ID can not be deleted!) {Environment.NewLine}Wrong ID is: {id}{Environment.NewLine}Press any button to continue...");
                                Console.ReadLine();
                                Console.Clear();
                            }
                        }
                        else
                        {
                            Console.WriteLine($"FAILED: ID can not be negative number! {Environment.NewLine}Wrong ID is: {id}{Environment.NewLine}Press any button to continue...");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                }
                else if (selectedItem == this.menuItems[3])
                {
                    Console.Clear();
                    string tableName = " ";
                    do
                    {
                        Console.WriteLine("Please chose one table! (Options: Vodka/Company/Delivery/Shop)");
                        tableName = Console.ReadLine();
                    }
                    while (!tableNames.Contains(tableName));
                    Console.Clear();
                    Console.WriteLine($"\nYou have chosen the {tableName} table");
                    if (tableName == "Vodka")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive us the new params of the selected vodka! (Search through IDs)\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        List<string> vodka = new List<string>();
                        Console.Write("ID: ");
                        vodka.Add(Console.ReadLine());
                        Console.Write("Company_ID: ");
                        vodka.Add(Console.ReadLine());
                        Console.Write("Name of Vodka: ");
                        vodka.Add(Console.ReadLine());
                        Console.Write("Number of Destills: ");
                        vodka.Add(Console.ReadLine());
                        Console.Write("Price: ");
                        vodka.Add(Console.ReadLine());
                        Console.Write("Volume: ");
                        vodka.Add(Console.ReadLine());
                        Console.Write("Flavoured: ");
                        vodka.Add(Console.ReadLine());
                        Console.Write("Material: ");
                        vodka.Add(Console.ReadLine());
                        Console.Write("Qualification: ");
                        vodka.Add(Console.ReadLine());
                        Console.WriteLine("\nLoading...\n");
                        if (vodkaLogic.UpdateVodka(int.Parse(vodka[0]), vodka))
                        {
                            Console.WriteLine("Table has been successfully updated!");
                            Console.ReadLine();
                            Console.Clear();
                        }
                        else
                        {
                            Console.WriteLine($"FAILED: Vodka can not find!{vodka[0]}");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                    else if (tableName == "Company")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive us the new params of the selected company! (Search through IDs)\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        List<string> company = new List<string>();
                        Console.Write("ID: ");
                        company.Add(Console.ReadLine());
                        Console.Write("Name: ");
                        company.Add(Console.ReadLine());
                        Console.Write("Country: ");
                        company.Add(Console.ReadLine());
                        Console.Write("Accessibility: ");
                        company.Add(Console.ReadLine());
                        Console.Write("Estabisment: ");
                        company.Add(Console.ReadLine());
                        Console.Write("CEO: ");
                        company.Add(Console.ReadLine());
                        Console.WriteLine("\nLoading...\n");
                        if (companyLogic.UpdateCompany(int.Parse(company[0]), company))
                        {
                            Console.WriteLine("Table has been successfully updated!");
                            Console.ReadLine();
                            Console.Clear();
                        }
                        else
                        {
                            Console.WriteLine($"FAILED: Vodka can not find!{company[0]}");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                    else if (tableName == "Delivery")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive us the new params of the selected delviery! (Search through IDs)\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        List<string> delivery = new List<string>();
                        Console.Write("ID: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Vodka_ID: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Shop_ID: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Date: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Status: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Quantity: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Client: ");
                        delivery.Add(Console.ReadLine());
                        Console.Write("Deadline: ");
                        delivery.Add(Console.ReadLine());
                        Console.WriteLine("\nLoading...\n");
                        if (deliveryLogic.UpdateDelivery(int.Parse(delivery[0]), delivery))
                        {
                            Console.WriteLine("Table has been successfully updated!");
                            Console.ReadLine();
                            Console.Clear();
                        }
                        else
                        {
                            Console.WriteLine($"FAILED: Vodka can not find!{delivery[0]}");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                    else if (tableName == "Shop")
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\t\t\tGive us the new params of the selected shop! (Search through IDs)\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        List<string> shop = new List<string>();
                        Console.Write("ID: ");
                        shop.Add(Console.ReadLine());
                        Console.Write("Name: ");
                        shop.Add(Console.ReadLine());
                        Console.Write("CEO: ");
                        shop.Add(Console.ReadLine());
                        Console.Write("Address: ");
                        shop.Add(Console.ReadLine());
                        Console.Write("Min Quantity: ");
                        shop.Add(Console.ReadLine());
                        Console.Write("District: ");
                        shop.Add(Console.ReadLine());
                        Console.WriteLine("\nLoading...\n");
                        if (shopLogic.UpdateShop(int.Parse(shop[0]), shop))
                        {
                            Console.WriteLine("Table has been successfully updated!");
                            Console.ReadLine();
                            Console.Clear();
                        }
                        else
                        {
                            Console.WriteLine($"FAILED: Vodka can not find!{shop[0]}");
                            Console.ReadLine();
                            Console.Clear();
                        }
                    }
                }
                else if (selectedItem == this.menuItems[4])
                {
                    Console.Clear();
                    Console.Write("Vodka Name: ");
                    string name = Console.ReadLine();
                    IQueryable<Delivery> q = deliveryLogic.GetDeliveriesUsingVodkaName(name);
                    if (q != null)
                    {
                        foreach (var item in q)
                        {
                            Console.WriteLine($"{item.Deadline} | {item.Delivery_ID} | {item.Date} | {item.Client} | {item.Quantity}");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"\nCan not find the vodka name({name})");
                    }

                    Console.WriteLine("\nPress any button to return...");
                    Console.ReadLine();
                    Console.Clear();
                }
                else if (selectedItem == this.menuItems[5])
                {
                    Console.Clear();
                    Console.Write("Company Name: ");
                    string name = Console.ReadLine();
                    IQueryable<Vodka> q = vodkaLogic.GetCompanyAndVodkaDatas(name).AsQueryable();
                    if (q != null)
                    {
                        foreach (var item in q)
                        {
                            Console.WriteLine($"{item.Vodka_Name} | {item.Price} | {item.Volume} | {item.Qualification} | {item.Material}");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"\nCompany name with ({name}) can not find!");
                    }

                    Console.WriteLine("\nPress any button to return...");
                    Console.ReadLine();
                    Console.Clear();
                }
                else if (selectedItem == this.menuItems[6])
                {
                    Console.Clear();
                    IQueryable<Company> q = companyLogic.GetExpensiveVodkas();
                    foreach (var item in q)
                    {
                        Console.WriteLine($"{item.Company_ID} | {item.Name} | {item.Establisment} | {item.Country} | {item.CEO} | {item.Accessibility}");
                    }

                    Console.WriteLine("\nPress any button to return...");
                    Console.ReadLine();
                    Console.Clear();
                }
                else if (selectedItem == this.menuItems[7])
                {
                    Console.Clear();
                    string getXML = endPointLogic.XmlFromURL();
                    endPointLogic.SplitXml(getXML);
                    Console.WriteLine(Environment.NewLine + "Download was successful!" + Environment.NewLine + "Press any button to continue...");
                    Console.ReadLine();
                    Console.Clear();
                }
                else if (selectedItem == this.menuItems[8])
                {
                    Console.Clear();
                    Console.WriteLine("Goodbye!");
                    Thread.Sleep(500);
                    Environment.Exit(0);
                }
            }
        }

        private string DrawMenu(List<string> menuItems)
        {
            for (int i = 0; i < menuItems.Count; i++)
            {
                if (i == this.index)
                {
                    Console.ForegroundColor = ConsoleColor.Green;

                    Console.WriteLine(menuItems[i]);
                }
                else
                {
                    Console.WriteLine(menuItems[i]);
                }

                Console.ResetColor();
            }

            ConsoleKeyInfo ckey = Console.ReadKey();

            if (ckey.Key == ConsoleKey.DownArrow)
            {
                if (this.index == menuItems.Count - 1)
                {
                    this.index = 0;
                }
                else
                {
                    this.index++;
                }
            }
            else if (ckey.Key == ConsoleKey.UpArrow)
            {
                if (this.index <= 0)
                {
                    this.index = menuItems.Count - 1;
                }
                else
                {
                    this.index--;
                }
            }
            else if (ckey.Key == ConsoleKey.Enter)
            {
                return menuItems[this.index];
            }
            else
            {
                return string.Empty;
            }

            Console.Clear();
            return string.Empty;
        }
    }
}
