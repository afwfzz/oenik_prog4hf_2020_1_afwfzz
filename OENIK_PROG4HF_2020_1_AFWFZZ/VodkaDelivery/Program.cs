﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Main Program.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            Menu menu = new Menu();
            menu.MenuOut();
        }
    }
}
