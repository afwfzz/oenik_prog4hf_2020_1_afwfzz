﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Pong___Video_Gyak
{
    class MyShape //GameItem Ősosztály
    {
        Rect area;
        public Rect Area        //NEM REFERENCIA TÍPUS CHANGEX METÓDUSBAN EZÉRT KELL AZ ADAT TAGOT MÓDOSÍTANI!! TULAJDONSÁG CSAK ARRA HOGY KÍVÜLRŐL LEHESSEN MÓDOSÍTANI
        {
            get { return area; }   // NO get;
        }

        public int Dx { get; set; }       //Sebesség vektor!!
        public int Dy { get; set; }

        public MyShape(double x, double y, double w, double h)
        {
            area = new Rect(x, y, w, h);
            Dx = 8;
            Dy = 8;
        }

        public void ChangeX(int diff)
        {
            area.X += diff;
        }
        public void ChangeY(int diff)
        {
            area.Y += diff;
        }
        public void SetXY(double x, double y)
        {
            area.X = x;
            area.Y = y;
        }
    }
}
