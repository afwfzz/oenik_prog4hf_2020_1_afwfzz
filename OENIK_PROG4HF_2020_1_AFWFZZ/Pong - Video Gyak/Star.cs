﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Pong___Video_Gyak
{
    class Star : MyShape
    {
        double n;
        double r;

        public Star(double x, double y, double r, double n)
            : base(x,y, 2*r, 2*r)
        {
            this.r = r;
            this.n = n;
        }

        public Geometry GetGeometry()
        {
            List<Point> points = new List<Point>();
            for (int i = 0; i < n; i++)
            {
                double angle = i * 2 * Math.PI / n;
                Point p = new Point(r * Math.Cos(angle), r * Math.Sin(angle));
                if (i%2 ==1)
                {
                    p.X *= 0.2;
                    p.Y *= 0.2;
                }

                p.X += r + Area.X;
                p.Y += r + Area.Y;
                points.Add(p);
            }

            StreamGeometry geo = new StreamGeometry();
            using (StreamGeometryContext ctx = geo.Open())
            {
                ctx.BeginFigure(points[0], true, true);
                ctx.PolyLineTo(points, true, true);
            }

            return geo;
        }
    }
}
