﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong___Video_Gyak
{
    class PongLogic  //Logica szól hogy frissülni kell
    {
        PongModel model;
        public enum Direction { Left, Right }
        public enum EnemyDirection { Up, Down, Left, Right, UpRight, UpLeft, DownRight, DownLeft }
        public event EventHandler RefreshScreen; //Logica szól a UI felé hogy kell a frissítés
        Random rnd;

        public PongLogic(PongModel model)
        {
            this.model = model;
        }

        public void MovePad(Direction d)
        {
            if (d == Direction.Left)
            {
                model.Pad.ChangeX(-10);
            }
            else
            {
                model.Pad.ChangeX(+10);
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        public void JumpPad(double x)
        {
            model.Pad.SetXY(x, model.Pad.Area.Y);
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        private bool MoveShape(MyShape shape)
        {
            bool IsFaulted = false;
            shape.ChangeX(shape.Dx);
            shape.ChangeY(shape.Dy);

            if (shape.Area.Left < 0 ||shape.Area.Right > Config.Width)
            {
                shape.Dx = -shape.Dx;
            }
            if (shape.Area.Top < 0 || shape.Area.IntersectsWith(model.Pad.Area))       //Ütközés vizsgálás
            {
                shape.Dy = -shape.Dy;
            }
            if (shape.Area.Bottom > Config.Height)
            {
                IsFaulted = true;
                shape.SetXY(shape.Area.X, Config.Height/2);
            }

            return IsFaulted;
        }

        public void Moveball()
        {
            if (MoveShape(model.Ball))  model.Errors++;
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        public void AddStar( )
        {
            model.Stars.Add(new Star(Config.Width/2, Config.Height/2, Config.BallSize/2, 6));
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        public void AddEnemy()
        {
            model.Enemies.Add(new Enemy(Config.Width/3,15,Config.EnemySize));
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        private bool EximeEnemy(Enemy enemy)
        {
            bool test = false;
            if (enemy.Area.Left < 0 || enemy.Area.Right > Config.Width)
            {
                enemy.Dx = -enemy.Dx;
            }
            if (enemy.Area.Top < 0 || enemy.Area.Bottom > Config.Height )       
            {
                enemy.Dy = -enemy.Dy;
            }
            if (enemy.Area.IntersectsWith(model.Ball.Area)) 
            {
                test = true;
            }
            return test;
        }

        public void MoveEnemy()
        {
            bool test = false;
            int i = 0;
            while (!test && i<model.Enemies.Count && model.Enemies != null)
            {
                test = ControlEnemy(model.Enemies[i]);
                i++;
            }
            if (true == test )
            {
                model.Enemies.Remove(model.Enemies[i-1]);
                BounceTheBall();
                RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        private bool ControlEnemy(Enemy shape)
        {
            EnemyDirection rnd;

            rnd = (EnemyDirection)new Random().Next(0, 5);
            if (rnd == EnemyDirection.Up)
            {
                shape.ChangeY(-shape.EnemyDY);
                if (shape.Area.Top < 0)
                {
                    shape.ChangeY(shape.EnemyDY * 2);
                }
            }
            else if (rnd == EnemyDirection.Down)
            {
                shape.ChangeY(shape.EnemyDY);
                if (shape.Area.Bottom > Config.Height)
                {
                    shape.ChangeY(-shape.EnemyDY * 2);
                }
            }
            else if (rnd == EnemyDirection.Left)
            {
                shape.ChangeX(-shape.EnemyDX);
                if (shape.Area.Left < 0)
                {
                    shape.ChangeX(shape.EnemyDX * 2);
                }
            }
            else if (rnd == EnemyDirection.Right)
            {
                shape.ChangeX(shape.EnemyDX);
                if (shape.Area.Right > Config.Width)
                {
                    shape.ChangeX(-shape.EnemyDX * 2);
                }
            }
            return EximeEnemy(shape);
        }

        private void BounceTheBall()
        {
            model.Ball.ChangeX(model.Ball.Dx);
            model.Ball.ChangeY(model.Ball.Dy);

            rnd = new Random();
            int direction = rnd.Next(0,2);
            if (direction == 1)
            {
                model.Ball.Dx = -model.Ball.Dx;
            }
            else
            {
                model.Ball.Dy = -model.Ball.Dy;
            }

            RefreshScreen?.Invoke(this, EventArgs.Empty);

        }

        public void SummonEnemy()
        {
            double x = 200;
                while (model.Enemies.Count != 3)
                {
                    model.Enemies.Add(new Enemy(x, 15, Config.EnemySize));
                    x += 200;
                    RefreshScreen?.Invoke(this, EventArgs.Empty);
                }
        }

        public void MoveStar()
        {
            
            foreach (Star star in model.Stars)
            {
                if (MoveShape(star)) model.Errors++;
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
    }
}
