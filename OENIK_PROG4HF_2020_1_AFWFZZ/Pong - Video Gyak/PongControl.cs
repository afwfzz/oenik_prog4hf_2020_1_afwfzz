﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace Pong___Video_Gyak
{
    class PongControl : FrameworkElement      //magába foglalja a modelt, a logicot és a renderert. Tényleges műveletek mindig a logicban
    { 
        //Control feladata az időzítések ellátása, automatikusan hívott metódusok hívása, ezért Timer
        PongModel model;
        PongLogic logic;
        PongRenderer renderer;
        DispatcherTimer tickTimer;

        public PongControl()
        {
            Loaded += PongControl_Loaded;  //+=<TAB><RET> ide SEMMI MÁS NEM KELL
        }

        private void PongControl_Loaded(object sender, RoutedEventArgs e)  //tényleges elrendezés megtörtént, tudjuk hogy ablako nvagyunk rajta
        {
            model = new PongModel();
            logic = new PongLogic(model);
            renderer = new PongRenderer(model);

            Window win = Window.GetWindow(this);
            if (win != null)   //pl timer kezelés, billentyűzet kezelés   TIMER csak akkor amikor ablakunk van
            {
                tickTimer = new DispatcherTimer();
                tickTimer.Interval = TimeSpan.FromMilliseconds(40);  // => 25fps
                tickTimer.Tick += TickTimer_Tick;
                tickTimer.Start();

                win.KeyDown += Win_KeyDown;
                MouseLeftButtonDown += PongControl_MouseLeftButtonDown;
            }

            logic.RefreshScreen += (obj,args) => InvalidateVisual();
            InvalidateVisual();
        }

        private void PongControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            logic.JumpPad(e.GetPosition(this).X);
        }

        private void Win_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left: logic.MovePad(PongLogic.Direction.Left); break;
                case Key.Right: logic.MovePad(PongLogic.Direction.Right); break;
                case Key.Space: logic.AddStar(); break;
            }
        }

        private void TickTimer_Tick(object sender, EventArgs e)
        {
            logic.SummonEnemy();
            logic.Moveball();
            logic.MoveStar();
            logic.MoveEnemy();
        }
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (renderer != null)
            {
                renderer.DrawThings(drawingContext);
            }
        }
    }
}
