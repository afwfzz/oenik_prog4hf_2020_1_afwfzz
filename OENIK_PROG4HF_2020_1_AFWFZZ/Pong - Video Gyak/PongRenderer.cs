﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Pong___Video_Gyak
{
    class PongRenderer //PongModelt megjelenítse egy rajzlapon
    {
        PongModel model;
        public PongRenderer(PongModel model)
        {
            this.model = model;
        }

        // 1. Ha mindent egy metódusba rakunk az bonyolít ->Builder design pattern
        // 2. DrawThings 20 fps -> 20or fut le másodpercenként -> LÉTFONTOSSÁGÚ hogy a DrawThings jó teljesítményű legyen -> ne itt legyenek a konstruktor hívások, amíg csak lehet adattaggénk egyszer a rendererben
        public void DrawThings(DrawingContext ctx) 
        {
            DrawingGroup dg = new DrawingGroup();

            GeometryDrawing background = new GeometryDrawing(Config.BgBrush, 
                new Pen(Config.BorderBrush, Config.BorderSize),
                new RectangleGeometry(new Rect(0,0, Config.Width, Config.Height)));        //Ezeket a ctor hívásokat illendő kerülni
            GeometryDrawing ball = new GeometryDrawing(Config.BallBgBrush,
                new Pen(Config.BallLineBrush,1),
                new EllipseGeometry(model.Ball.Area));                   //Azt tárolja ahol a labda van jelenleg, oda kell kirajzolnia
            GeometryDrawing pad = new GeometryDrawing(Config.PadBgBrush, 
                new Pen(Config.PadLineBrush,1),
                new RectangleGeometry(model.Pad.Area));
            FormattedText formattedText = new FormattedText(model.Errors.ToString(),
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Arial"),
                16,
                Brushes.Black);
            GeometryDrawing text = new GeometryDrawing(null, new Pen(Brushes.Red,3), 
                formattedText.BuildGeometry(new Point(5,5)));

            dg.Children.Add(background);
            dg.Children.Add(ball);
            dg.Children.Add(pad);
            dg.Children.Add(text);

            foreach (Enemy enemy in model.Enemies)
            {
                GeometryDrawing enemygeo = new GeometryDrawing(Config.EnemyBgBrush,
                new Pen(Config.EnemyLineBrush, 1),
                new RectangleGeometry(enemy.Area));
                
                dg.Children.Add(enemygeo);
            }
            

            foreach (Star star in model.Stars)
            {
                GeometryDrawing stargeo = new GeometryDrawing(Config.BallBgBrush,
                    new Pen(Config.BallLineBrush,1),
                    star.GetGeometry());

                dg.Children.Add(stargeo);
            }

            ctx.DrawDrawing(dg);
        }
    }
}
