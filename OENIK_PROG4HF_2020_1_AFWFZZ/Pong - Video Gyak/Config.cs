﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Pong___Video_Gyak
{
    public class Config   //SOHA nem fog változni!!! Ami változik megy a Modelbe->
    {
        public static Brush BorderBrush = Brushes.DarkGray; 
        public static Brush BgBrush = Brushes.Cyan; 

        public static Brush BallBgBrush = Brushes.Yellow; 
        public static Brush BallLineBrush = Brushes.Red; 
        public static Brush PadBgBrush = Brushes.Brown; 
        public static Brush PadLineBrush = Brushes.Black;

        public static double Width = 700;
        public static double Height = 300;
        public static int BorderSize = 4;

        public static int BallSize = 20;
        public static int PadWidth = 100;
        public static int PadHeight = 20;
        public static double EnemySize = 30;

        public static Brush EnemyBgBrush = Brushes.DarkBlue;
        public static Brush EnemyLineBrush = Brushes.Black;
    }
}
