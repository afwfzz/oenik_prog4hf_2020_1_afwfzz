﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong___Video_Gyak
{
    class PongModel //Ami a játéktéren módosuló dolog
    {
        public int Errors { get; set; }
        public MyShape Pad { get; set; }
        public MyShape Ball { get; set; }
        public List<Enemy> Enemies { get; private set; }  
        public List<Star> Stars { get; private set; }

        public PongModel()
        {
            Ball = new MyShape(Config.Width/2, Config.Height/2 ,Config.BallSize , Config.BallSize);
            Pad = new MyShape(Config.Width / 2, Config.Height - Config.PadHeight, Config.PadWidth, Config.PadHeight);
            Stars = new List<Star>();
            Enemies = new List<Enemy>();
        }
    }
}
