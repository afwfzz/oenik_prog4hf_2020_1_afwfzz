﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Pong___Video_Gyak
{
    class Enemy : MyShape
    {
        public Enemy(double x, double y, double s) :base(x, y, s, s)
        {
            EnemyDX = 6;
            EnemyDY = 6;
        }

        public int EnemyDX { get; set; }
        public int EnemyDY { get; set; }

    }
}
