﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VodkaDelivery.Wpf
{
    class VodkaVM : ObservableObject
    {
		private int iD;
		private int company_ID;
		private string vodkaName;
		private int numberOfDestills;
		private int price;
		private float volume;
		private string flavoured;
		private string material;
		private int qualification;

		public int Qualification
		{
			get { return qualification; }
			set { Set(ref qualification, value); }
		}


		public string Material
		{
			get { return material; }
			set { Set(ref material, value); }
		}


		public string Flavoured
		{
			get { return flavoured; }
			set { Set(ref flavoured, value); }
		}


		public float Volume
		{
			get { return volume; }
			set { Set(ref volume, value); }
		}


		public int Price
		{
			get { return price; }
			set { Set(ref price, value); }
		}


		public int NumberOfDestills
		{
			get { return numberOfDestills; }
			set { Set(ref numberOfDestills, value); }
		}


		public string VodkaName
		{
			get { return vodkaName; }
			set { Set(ref vodkaName, value); }
		}



		public int Company_ID
		{
			get { return company_ID; }
			set { Set(ref company_ID, value); }
		}


		public int ID
		{
			get { return iD; }
			set { Set(ref iD, value); }
		}

		public void CopyFrom(VodkaVM other)
		{
			if (other == null) return;
			this.ID = other.ID;
			this.Company_ID = other.Company_ID;
			this.VodkaName = other.VodkaName;
			this.NumberOfDestills = other.NumberOfDestills;
			this.Price = other.Price;
			this.Volume = other.Volume;
			this.Flavoured = other.Flavoured;
			this.Material = other.Material;
			this.Qualification = other.Qualification;
		}

	}
}
