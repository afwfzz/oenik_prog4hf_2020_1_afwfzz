﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace VodkaDelivery.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:49389/api/VodkaApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "VodkaResult");
        }

        public List<VodkaVM> ApiGetVodkas()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<VodkaVM>>(json);
            //SendMessage(true);
            return list;
        }

        public void ApiDelVodka(VodkaVM vodka)
        {
            bool success = false;
            if (vodka != null)
            {
                string json = client.GetStringAsync(url + "del/" + vodka.ID).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditVodka(VodkaVM vodka, bool isEditing)
        {
            if (vodka == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";
            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(VodkaVM.ID), vodka.ID.ToString());
            postData.Add(nameof(VodkaVM.Company_ID), vodka.Company_ID.ToString());
            postData.Add(nameof(VodkaVM.VodkaName), vodka.VodkaName);
            postData.Add(nameof(VodkaVM.NumberOfDestills), vodka.NumberOfDestills.ToString());
            postData.Add(nameof(VodkaVM.Price), vodka.Price.ToString());
            postData.Add(nameof(VodkaVM.Volume), vodka.Volume.ToString());
            postData.Add(nameof(VodkaVM.Flavoured), vodka.Flavoured.ToString());
            postData.Add(nameof(VodkaVM.Material), vodka.Material.ToString());
            postData.Add(nameof(VodkaVM.Qualification), vodka.Qualification.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditVodka(VodkaVM vodka, Func<VodkaVM, bool> editor)
        {
            VodkaVM clone = new VodkaVM();
            if (vodka != null) clone.CopyFrom(vodka);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (vodka != null) success = ApiEditVodka(vodka, true);
                else success = ApiEditVodka(clone, false);
            }
            SendMessage(success == true);
        }
    }
}
