﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VodkaDelivery.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
		private VodkaVM selectedVodka;
		private ObservableCollection<VodkaVM> allVodkas;

		public ObservableCollection<VodkaVM> AllVodkas
		{
			get { return allVodkas; }
			set { Set(ref allVodkas, value); }
		}

		public VodkaVM SelectedVodka
		{
			get { return selectedVodka; }
			set { Set(ref selectedVodka, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<VodkaVM, bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();
			DelCmd = new RelayCommand(() => logic.ApiDelVodka(selectedVodka));
			AddCmd = new RelayCommand(() => logic.EditVodka(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditVodka(selectedVodka, EditorFunc));
			LoadCmd = new RelayCommand(() => 
			AllVodkas = new ObservableCollection<VodkaVM>(logic.ApiGetVodkas()));
		}
	}
}
