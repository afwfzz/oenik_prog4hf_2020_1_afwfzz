﻿// <copyright file="VodkaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;
    using VodkaDelivery.Repository;

    /// <summary>
    /// Vodka BL class of Program.
    /// </summary>
    public class VodkaLogic : IVodkaLogic
    {
        /// <summary>
        /// Using for DI.
        /// </summary>
        private readonly IVodkaRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="VodkaLogic"/> class.
        /// </summary>
        /// <param name="repo">Using for DI.</param>
        public VodkaLogic(IVodkaRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Return one vodka item.
        /// </summary>
        /// <param name="id">Id of vodka.</param>
        /// <returns>Return one Vodka.</returns>
        public Vodka GetOne(int id)
        {
            return this.repo.GetOne(id);
        }

        /// <summary>
        /// Method that create a table from vodkas.
        /// </summary>
        /// <returns>Table from vodkas.</returns>
        public IList<Vodka> GetVodkas()
        {
            return this.repo.GetAll().AsQueryable().ToList();
        }

        /// <summary>
        /// Insert into vodka table.
        /// </summary>
        /// <param name="newVodka">One vodka element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        public bool InsertIntoVodka(Vodka newVodka)
        {
            return this.repo.InsertIn(newVodka);
        }

        /// <summary>
        /// Update one element from vodka table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="updatedVodka">new datails.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        public bool UpdateVodka(int id, List<string> updatedVodka)
        {
            return this.repo.Update(id, updatedVodka);
        }

        /// <summary>
        /// Delete one element from vodka table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>>True if could be deleted, false if could not.</returns>
        public bool DeleteVodka(int id)
        {
            return this.repo.Delete(id);
        }

        /// <summary>
        /// Get the vodka datas and its company is datas.
        /// </summary>
        /// <param name="name">Search through names.</param>
        /// <returns>Vodkas and companies datas if its found it, null if not.</returns>
        public IQueryable<Vodka> GetCompanyAndVodkaDatas(string name)
        {
            return this.repo.GetCompanyAndVodkaDatas(name);
        }
    }
}
