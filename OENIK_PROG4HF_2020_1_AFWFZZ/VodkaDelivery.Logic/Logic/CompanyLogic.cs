﻿// <copyright file="CompanyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;
    using VodkaDelivery.Repository;

    /// <summary>
    /// Company BL class of Program.
    /// </summary>
    public class CompanyLogic : ICompanyLogic
    {
        /// <summary>
        /// Using for DI.
        /// </summary>
        private readonly ICompanyRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyLogic"/> class.
        /// </summary>
        /// <param name="repo">Using for DI.</param>
        public CompanyLogic(ICompanyRepository repo)
        {
            this.repo = repo;
        }

        /// <inheritdoc/>
        public Company GetOne(int id)
        {
            return this.repo.GetOne(id);
        }

        /// <summary>
        /// Method that create a table from companies.
        /// </summary>
        /// <returns>Table from company.</returns>
        public List<string> GetCompanies()
        {
            IQueryable<Company> companies = this.repo.GetAll().AsQueryable();
            List<string> list = new List<string>();
            List<string> back = list;
            back.Add($"COMPANY_ID | NAME | COUNTRY | ACCESSIBILITY | ESTABLISMENT | CEO\n");
            foreach (Company item in companies)
            {
                if (item.Company_ID < 10)
                {
                    back.Add($"{item.Company_ID}  | {item.Name} | {item.Country} | {item.Accessibility} | {item.Establisment} | {item.CEO}");
                }
                else
                {
                    back.Add($"{item.Company_ID} | {item.Name} | {item.Country} | {item.Accessibility} | {item.Establisment} | {item.CEO}");
                }
            }

            return back;
        }

        /// <summary>
        /// Insert into company table.
        /// </summary>
        /// <param name="newCompany">One company element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        public bool InsertIntoCompany(List<string> newCompany)
        {
            Company company = new Company();
            Company inCompany = company;
            inCompany.Company_ID = int.Parse(newCompany[0]);
            inCompany.Name = newCompany[1];
            inCompany.Country = newCompany[2];
            inCompany.Accessibility = newCompany[3];
            inCompany.Establisment = int.Parse(newCompany[4]);
            inCompany.CEO = newCompany[5];
            return this.repo.InsertIn(inCompany);
        }

        /// <summary>
        /// Update one element from company table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="updatedCompany">new datails.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        public bool UpdateCompany(int id, List<string> updatedCompany)
        {
            return this.repo.Update(id, updatedCompany);
        }

        /// <summary>
        /// Delete one element from company table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>>True if could be deleted, false if could not.</returns>
        public bool DeteleCompany(int id)
        {
            return this.repo.Delete(id);
        }

        /// <summary>
        /// Return expensive vodkas.
        /// </summary>
        /// <returns>IQueryable of vodkas.</returns>
        public IQueryable<Company> GetExpensiveVodkas()
        {
            return this.repo.GetExpensiveVodkas();
        }
    }
}
