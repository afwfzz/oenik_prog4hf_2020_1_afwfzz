﻿// <copyright file="DeliveryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;
    using VodkaDelivery.Repository;

    /// <summary>
    /// Delivery BL class of Program.
    /// </summary>
    public class DeliveryLogic : IDeliveryLogic
    {
        /// <summary>
        /// Using for DI.
        /// </summary>
        private readonly IDeliveryRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliveryLogic"/> class.
        /// </summary>
        /// <param name="repo">Using for DI.</param>
        public DeliveryLogic(IDeliveryRepository repo)
        {
            this.repo = repo;
        }

        /// <inheritdoc/>
        public Delivery GetOne(int id)
        {
            return this.repo.GetOne(id);
        }

        /// <summary>
        /// Method that create a table from companies.
        /// </summary>
        /// <returns>Table from company.</returns>
        public List<string> GetDeliveries()
        {
            IQueryable<Delivery> deliveries = this.repo.GetAll().AsQueryable();
            List<string> list = new List<string>();
            List<string> back = list;
            back.Add($"COMPANY_ID | NAME | COUNTRY | ACCESSIBILITY | ESTABLISMENT | CEO\n");
            foreach (Delivery item in deliveries)
            {
                if (item.Delivery_ID < 10)
                {
                    back.Add($"{item.Delivery_ID}  | {item.Vodka_ID} | {item.Shop_ID} | {item.Date} | {item.Status} | {item.Quantity} | {item.Client} | {item.Deadline}");
                }
                else
                {
                    back.Add($"{item.Delivery_ID} | {item.Vodka_ID} | {item.Shop_ID} | {item.Date} | {item.Status} | {item.Quantity} | {item.Client} | {item.Deadline}");
                }
            }

            return back;
        }

        /// <summary>
        /// Insert into delivery table.
        /// </summary>
        /// <param name="newDelivery">One delivery element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        public bool InsertIntoDelivery(List<string> newDelivery)
        {
            Delivery delivery = new Delivery();
            Delivery inDelivery = delivery;
            inDelivery.Delivery_ID = int.Parse(newDelivery[0]);
            inDelivery.Vodka_ID = int.Parse(newDelivery[1]);
            inDelivery.Shop_ID = int.Parse(newDelivery[2]);
            inDelivery.Date = DateTime.Parse(newDelivery[3]);
            inDelivery.Status = newDelivery[4];
            inDelivery.Quantity = int.Parse(newDelivery[5]);
            inDelivery.Client = newDelivery[6];
            inDelivery.Deadline = DateTime.Parse(newDelivery[7]);
            return this.repo.InsertIn(inDelivery);
        }

        /// <summary>
        /// Update one element from delivery table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="updatedDelivery">new datails.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        public bool UpdateDelivery(int id, List<string> updatedDelivery)
        {
            return this.repo.Update(id, updatedDelivery);
        }

        /// <summary>
        /// Delete one element from delviery table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>>True if could be deleted, false if could not.</returns>
        public bool DeteleDelivery(int id)
        {
            return this.repo.Delete(id);
        }

        /// <summary>
        /// List of vodka's name and when were deliveried them.
        /// </summary>
        /// <param name="name">Name of vodka.</param>
        /// <returns>Vodka's name and.</returns>
        public IQueryable<Delivery> GetDeliveriesUsingVodkaName(string name)
        {
            return this.repo.GetDeliveriesUsingVodkaName(name);
        }
    }
}
