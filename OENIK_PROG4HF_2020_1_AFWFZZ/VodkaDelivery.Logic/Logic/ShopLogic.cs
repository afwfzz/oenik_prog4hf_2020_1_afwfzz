﻿// <copyright file="ShopLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;
    using VodkaDelivery.Repository;

    /// <summary>
    /// Shop BL class of Program.
    /// </summary>
    public class ShopLogic : IShopLogic
    {
        /// <summary>
        /// Using for DI.
        /// </summary>
        private readonly IShopRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShopLogic"/> class.
        /// </summary>
        /// <param name="repo">Using for DI.</param>
        public ShopLogic(IShopRepository repo)
        {
            this.repo = repo;
        }

        /// <inheritdoc/>
        public Shop GetOne(int id)
        {
            return this.repo.GetOne(id);
        }

        /// <summary>
        /// Method that create a table from shops.
        /// </summary>
        /// <returns>Table from shops.</returns>
        public List<string> GetShops()
        {
            IQueryable<Shop> shops = this.repo.GetAll().AsQueryable();
            List<string> list = new List<string>();
            List<string> back = list;
            back.Add($"SHOP_ID | NAME | CEO | ADDRESS | MIN_QUANTITY | DISTRICT");
            foreach (Shop item in shops)
            {
                back.Add($"{item.Shop_ID} | {item.Name} | {item.CEO} | {item.Address} | {item.Min_Quantity} | {item.District}");
            }

            return back;
        }

        /// <summary>
        /// Insert into shop table.
        /// </summary>
        /// <param name="newShop">One shop element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        public bool InsertIntoShop(List<string> newShop)
        {
            Shop inShop = new Shop
            {
                Shop_ID = int.Parse(newShop[0]),
                Name = newShop[1],
                CEO = newShop[2],
                Address = newShop[3],
                Min_Quantity = int.Parse(newShop[4]),
                District = newShop[5],
            };
            return this.repo.InsertIn(inShop);
        }

        /// <summary>
        /// Update one element from shop table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="updatedShop">new datails.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        public bool UpdateShop(int id, List<string> updatedShop)
        {
            return this.repo.Update(id, updatedShop);
        }

        /// <summary>
        /// Delete one element from shop table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>>True if could be deleted, false if could not.</returns>
        public bool DeleteShop(int id)
        {
            return this.repo.Delete(id);
        }
    }
}
