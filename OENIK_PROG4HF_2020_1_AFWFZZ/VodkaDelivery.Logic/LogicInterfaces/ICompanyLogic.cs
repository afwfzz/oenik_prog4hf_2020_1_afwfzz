﻿// <copyright file="ICompanyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;

    /// <summary>
    /// Interface for Company BL.
    /// </summary>
    public interface ICompanyLogic
    {
        Company GetOne(int id);

        /// <summary>
        /// Method that create a table from companies.
        /// </summary>
        /// <returns>List of companies.</returns>
        List<string> GetCompanies();

        /// <summary>
        /// Insert into company table.
        /// </summary>
        /// <param name="newCompany">One company element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        bool InsertIntoCompany(List<string> newCompany);

        /// <summary>
        /// Update one element from company table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="updatedCompany">new datails.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        bool UpdateCompany(int id, List<string> updatedCompany);

        /// <summary>
        /// Delete one element from company table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>>True if could be deleted, false if could not.</returns>
        bool DeteleCompany(int id);

        /// <summary>
        /// Return expensive vodkas.
        /// </summary>
        /// <returns>IQueryable of vodkas.</returns>
        IQueryable<Company> GetExpensiveVodkas();
    }
}
