﻿// <copyright file="IDeliveryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;

    /// <summary>
    /// Interface for Delivery BL.
    /// </summary>
    public interface IDeliveryLogic
    {
        Delivery GetOne(int id);

        /// <summary>
        /// Method that create a table from deliveries.
        /// </summary>
        /// <returns>List of deliveries.</returns>
        List<string> GetDeliveries();

        /// <summary>
        /// Insert into delivery table.
        /// </summary>
        /// <param name="newDelivery">One delivery element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        bool InsertIntoDelivery(List<string> newDelivery);

        /// <summary>
        /// Update one element from delivery table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="updatedDelivery">new datails.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        bool UpdateDelivery(int id, List<string> updatedDelivery);

        /// <summary>
        /// Delete one element from delviery table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>>True if could be deleted, false if could not.</returns>
        bool DeteleDelivery(int id);

        /// <summary>
        /// List of vodka's name and when were deliveried them.
        /// </summary>
        /// <param name="name">Name of vodka.</param>
        /// <returns>Vodka's name and.</returns>
        IQueryable<Delivery> GetDeliveriesUsingVodkaName(string name);
    }
}
