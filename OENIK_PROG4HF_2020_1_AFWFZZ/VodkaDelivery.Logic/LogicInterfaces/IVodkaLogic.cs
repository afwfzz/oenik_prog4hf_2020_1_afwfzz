﻿// <copyright file="IVodkaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using VodkaDelivery.Data;

    /// <summary>
    /// Interface fo Vodka BL.
    /// </summary>
    public interface IVodkaLogic
    {
        Vodka GetOne(int id);

        /// <summary>
        /// Method that create a table from vodkas.
        /// </summary>
        /// <returns>List of vodkas.</returns>
        IList<Vodka> GetVodkas();

        /// <summary>
        /// Insert into vodka table.
        /// </summary>
        /// <param name="newVodka">One vodka element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        bool InsertIntoVodka(Vodka newVodka);

        /// <summary>
        /// Update one element from vodka table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="updatedVodka">new datails.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        bool UpdateVodka(int id, List<string> updatedVodka);

        /// <summary>
        /// Delete one element from vodka table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>>True if could be deleted, false if could not.</returns>
        bool DeleteVodka(int id);

        /// <summary>
        /// Get the vodka datas and its company is datas.
        /// </summary>
        /// <param name="name">Search through names.</param>
        /// <returns>Vodkas and companies datas if its found it, null if not.</returns>
        IQueryable<Vodka> GetCompanyAndVodkaDatas(string name);
    }
}
