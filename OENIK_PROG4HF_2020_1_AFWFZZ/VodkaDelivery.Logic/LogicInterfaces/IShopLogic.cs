﻿// <copyright file="IShopLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;

    /// <summary>
    /// Interface for Shop BL.
    /// </summary>
    public interface IShopLogic
    {
        Shop GetOne(int id);

        /// <summary>
        /// Method that create a table from shops.
        /// </summary>
        /// <returns>List of shops.</returns>
        List<string> GetShops();

        /// <summary>
        /// Insert into shop table.
        /// </summary>
        /// <param name="newShop">One shop element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        bool InsertIntoShop(List<string> newShop);

        /// <summary>
        /// Update one element from shop table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="updatedShop">new datails.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        bool UpdateShop(int id, List<string> updatedShop);

        /// <summary>
        /// Delete one element from shop table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>>True if could be deleted, false if could not.</returns>
        bool DeleteShop(int id);
    }
}
