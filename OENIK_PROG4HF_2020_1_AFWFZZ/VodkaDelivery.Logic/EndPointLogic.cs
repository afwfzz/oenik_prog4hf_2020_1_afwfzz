﻿// <copyright file="EndPointLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;
    using VodkaDelivery.Repository;

    /// <summary>
    /// Logic class of the Program, for Endpoint.
    /// </summary>
    public class EndPointLogic
    {
        /// <summary>
        /// Get XML from java project.
        /// </summary>
        /// <returns>XML datas in string.</returns>
        public string XmlFromURL()
        {
            string remoteUri = "http://localhost:8080/OENIK_PROG3_2019_2_AFWFZZ/Delivery?status=Accepted";
            WebClient myWebClient = new WebClient();
            byte[] myDataBuffer = myWebClient.DownloadData(remoteUri);
            string download = Encoding.ASCII.GetString(myDataBuffer);
            return download;
        }

        /// <summary>
        /// Split XML to form.
        /// </summary>
        /// <param name="tombSplit">XML datas in string.</param>
        public void SplitXml(string tombSplit)
        {
            string[] toOut = new string[5];
            toOut[0] = tombSplit.Split('\n')[2].Split('<')[1].Split('>')[0] + ':' + ' ' + tombSplit.Split('\n')[2].Split('>')[1].Split('<')[0];
            toOut[1] = tombSplit.Split('\n')[3].Split('<')[1].Split('>')[0] + ':' + ' ' + tombSplit.Split('\n')[3].Split('>')[1].Split('<')[0];
            toOut[2] = tombSplit.Split('\n')[4].Split('<')[1].Split('>')[0] + ':' + ' ' + tombSplit.Split('\n')[4].Split('>')[1].Split('<')[0];
            toOut[3] = tombSplit.Split('\n')[5].Split('<')[1].Split('>')[0] + ':' + ' ' + tombSplit.Split('\n')[5].Split('>')[1].Split('<')[0];
            toOut[4] = tombSplit.Split('\n')[6].Split('<')[1].Split('>')[0] + ':' + ' ' + tombSplit.Split('\n')[6].Split('>')[1].Split('<')[0].Split('T')[0];

            for (int i = 0; i < toOut.Length; i++)
            {
                Console.WriteLine(toOut[i]);
            }
        }
    }
}
