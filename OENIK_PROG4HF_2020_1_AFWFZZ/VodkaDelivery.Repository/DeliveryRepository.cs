﻿// <copyright file="DeliveryRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;

    /// <summary>
    /// Delivery Repository.
    /// </summary>
    public class DeliveryRepository : IDeliveryRepository
    {
        private readonly vodkadeliverydatabaseEntities entities = new vodkadeliverydatabaseEntities();

        public Delivery GetOne(int id)
        {
            return this.entities.Delivery.SingleOrDefault(x => x.Delivery_ID == id);
        }

        /// <summary>
        /// Get all datas about deliveries.
        /// </summary>
        /// <returns>deliveries.</returns>
        public IQueryable<Delivery> GetAll()
        {
            var q = this.entities.Delivery.Select(x => x);
            return q.AsQueryable();
        }

        /// <summary>
        /// Insert into delivery table.
        /// </summary>
        /// <param name="newElement">One delivery element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        public bool InsertIn(Delivery newElement)
        {
            this.entities.Delivery.Add(newElement);
            this.Save();
            return true;
        }

        /// <summary>
        /// Update one element from delivery table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="elementoftable">New details.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        public bool Update(int id, List<string> elementoftable)
        {
            if (this.IsIn(id))
            {
                Delivery oldDelivery = this.entities.Delivery.Single(x => x.Delivery_ID == id);
                this.DeliveryforUpdate(oldDelivery, elementoftable);
                this.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Delete one element from delivery table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>True if could be deleted, false if could not.</returns>
        public bool Delete(int id)
        {
            if (id <= this.entities.Delivery.Count())
            {
                Delivery toDelete = this.entities.Delivery.Single(x => x.Delivery_ID == id);
                this.entities.Delivery.Remove(toDelete);
                this.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// List of vodka's name and when were deliveried them.
        /// </summary>
        /// <param name="name">Name of vodka.</param>
        /// <returns>Vodka's name and.</returns>
        public IQueryable<Delivery> GetDeliveriesUsingVodkaName(string name)
        {
            VodkaRepository repoJump = new VodkaRepository(this.entities);
            List<Delivery> deliveries = this.GetAll().ToList();
            List<Vodka> vodkas = repoJump.GetAll().ToList();

            var q = from delivery in deliveries
                    join vodka in vodkas on delivery.Vodka_ID equals vodka.Vodka_ID
                    where vodka.Vodka_Name == name
                    orderby delivery.Deadline
                    select delivery;

            return q.AsQueryable();
        }

        /// <summary>
        /// Check if table has element with that ID.
        /// </summary>
        /// <param name="id">Input ID.</param>
        /// <returns>True if tha ID is valid, False if not.</returns>
        public bool IsIn(int id)
        {
            if (id <= this.entities.Delivery.Count())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void DeliveryforUpdate(Delivery oldDelivery, List<string> elementoftable)
        {
            if (elementoftable[0] != string.Empty)
            {
                oldDelivery.Delivery_ID = int.Parse(elementoftable[0]);
            }

            if (elementoftable[1] != string.Empty)
            {
                oldDelivery.Vodka_ID = int.Parse(elementoftable[1]);
            }

            if (elementoftable[2] != string.Empty)
            {
                oldDelivery.Shop_ID = int.Parse(elementoftable[2]);
            }

            if (elementoftable[3] != string.Empty)
            {
                oldDelivery.Date = DateTime.Parse(elementoftable[3]);
            }

            if (elementoftable[4] != string.Empty)
            {
                oldDelivery.Status = elementoftable[4];
            }

            if (elementoftable[5] != string.Empty)
            {
                oldDelivery.Quantity = int.Parse(elementoftable[5]);
            }

            if (elementoftable[5] != string.Empty)
            {
                oldDelivery.Client = elementoftable[6];
            }

            if (elementoftable[5] != string.Empty)
            {
                oldDelivery.Deadline = DateTime.Parse(elementoftable[7]);
            }
        }

        private void Save()
        {
            this.entities.SaveChanges();
        }
    }
}
