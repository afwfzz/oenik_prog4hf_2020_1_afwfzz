﻿// <copyright file="ShopRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;

    /// <summary>
    /// Shop Repository.
    /// </summary>
    public class ShopRepository : IShopRepository
    {
        private readonly vodkadeliverydatabaseEntities entities = new vodkadeliverydatabaseEntities();

        public Shop GetOne(int id)
        {
            return this.entities.Shop.SingleOrDefault(x => x.Shop_ID == id);
        }

        /// <summary>
        /// Get all datas about shops.
        /// </summary>
        /// <returns>shops.</returns>
        public IQueryable<Shop> GetAll()
        {
            var q = this.entities.Shop.Select(x => x);
            return q.AsQueryable();
        }

        /// <summary>
        /// Insert into shop table.
        /// </summary>
        /// <param name="newElement">One shop element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        public bool InsertIn(Shop newElement)
        {
            this.entities.Shop.Add(newElement);
            this.Save();
            return true;
        }

        /// <summary>
        /// Update one element from shop table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="elementoftable">New details.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        public bool Update(int id, List<string> elementoftable)
        {
            if (this.IsIn(id))
            {
                Shop oldShop = this.entities.Shop.Single(x => x.Shop_ID == id);
                this.ShopforUpdate(oldShop, elementoftable);
                this.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Delete one element from shop table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>True if could be deleted, false if could not.</returns>
        public bool Delete(int id)
        {
            if (id <= this.entities.Shop.Count())
            {
                Shop toDelete = this.entities.Shop.Single(x => x.Shop_ID == id);
                this.entities.Shop.Remove(toDelete);
                this.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check if table has element with that ID.
        /// </summary>
        /// <param name="id">Input ID.</param>
        /// <returns>True if tha ID is valid, False if not.</returns>
        public bool IsIn(int id)
        {
            if (id <= this.entities.Shop.Count())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void ShopforUpdate(Shop oldShop, List<string> elementoftable)
        {
            if (elementoftable[0] != string.Empty)
            {
                oldShop.Shop_ID = int.Parse(elementoftable[0]);
            }

            if (elementoftable[1] != string.Empty)
            {
                oldShop.Name = elementoftable[1];
            }

            if (elementoftable[2] != string.Empty)
            {
                oldShop.CEO = elementoftable[2];
            }

            if (elementoftable[3] != string.Empty)
            {
                oldShop.Address = elementoftable[3];
            }

            if (elementoftable[4] != string.Empty)
            {
                oldShop.Min_Quantity = int.Parse(elementoftable[4]);
            }

            if (elementoftable[5] != string.Empty)
            {
                oldShop.District = elementoftable[5];
            }
        }

        private void Save()
        {
            this.entities.SaveChanges();
        }
    }
}
