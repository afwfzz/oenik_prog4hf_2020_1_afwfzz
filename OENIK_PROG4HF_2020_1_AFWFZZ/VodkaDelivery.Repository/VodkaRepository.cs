﻿// <copyright file="VodkaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;

    /// <summary>
    /// Vodka Repository.
    /// </summary>
    public class VodkaRepository : IVodkaRepository
    {
        private readonly vodkadeliverydatabaseEntities entities;

        public VodkaRepository(vodkadeliverydatabaseEntities entities)
        {
            this.entities = entities;
        }

        public Vodka GetOne(int id)
        {
            return this.entities.Vodka.SingleOrDefault(x => x.Vodka_ID == id);
        }

        /// <summary>
        /// Visszaadja a vodkákat.
        /// </summary>
        /// <returns>Vodka.</returns>
        public IQueryable<Vodka> GetAll()
        {
            return this.entities.Vodka.Select(x => x).AsQueryable<Vodka>();
        }

        /// <summary>
        /// Insert into vodka table.
        /// </summary>
        /// <param name="newElement">One vodka element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        public bool InsertIn(Vodka newElement)
        {
            this.entities.Vodka.Add(newElement);
            this.Save();
            return true;
        }

        /// <summary>
        /// Update one vodka element.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="elementoftable">New details.</param>
        /// <returns>True if there is a vodka for update|False if there is NO vodka for update.</returns>
        public bool Update(int id, List<string> elementoftable)
        {
            if (this.IsIn(id))
            {
                Vodka oldVodka = this.entities.Vodka.Single(x => x.Vodka_ID == id);
                this.VodkaforUpdate(oldVodka, elementoftable);
                this.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Delete on element.
        /// </summary>
        /// <param name="id">Seach through ids.</param>
        /// <returns>True if could be deleted, false if could not.</returns>
        public bool Delete(int id)
        {
            if (this.IsIn(id))
            {
                Vodka toDelete = this.entities.Vodka.Single(x => x.Vodka_ID == id);
                this.entities.Vodka.Remove(toDelete);
                this.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get the vodka datas and its company is datas.
        /// </summary>
        /// <param name="name">Search through names.</param>
        /// <returns>Vodkas and companies datas if its found it, null if not.</returns>
        public IQueryable<Vodka> GetCompanyAndVodkaDatas(string name)
        {
            CompanyRepository repoJump = new CompanyRepository();
            List<Vodka> vodkas = this.GetAll().ToList();
            List<Company> companies = repoJump.GetAll().ToList();

            var q = from vodka in vodkas
                    join company in companies on vodka.Company_ID equals company.Company_ID
                    where company.Name == name
                    orderby vodka.Vodka_Name
                    select vodka;

            return q.AsQueryable();
        }

        /// <summary>
        /// Check if table has element with that ID.
        /// </summary>
        /// <param name="id">Input ID.</param>
        /// <returns>True if tha ID is valid, False if not.</returns>
        public bool IsIn(int id)
        {
            if (id <= this.entities.Vodka.Count())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Change the updated vodka params.
        /// </summary>
        /// <param name="oldVodka">Old vodka.</param>
        /// <param name="elementoftable">List of details of new vodka.</param>
        private void VodkaforUpdate(Vodka oldVodka, List<string> elementoftable)
        {
            if (elementoftable[0] != string.Empty)
            {
                oldVodka.Vodka_ID = int.Parse(elementoftable[0]);
            }

            if (elementoftable[1] != string.Empty)
            {
                oldVodka.Company_ID = int.Parse(elementoftable[1]);
            }

            if (elementoftable[2] != string.Empty)
            {
                oldVodka.Vodka_Name = elementoftable[2];
            }

            if (elementoftable[3] != string.Empty)
            {
                oldVodka.Number_of_Destills = int.Parse(elementoftable[3]);
            }

            if (elementoftable[4] != string.Empty)
            {
                oldVodka.Price = int.Parse(elementoftable[4]);
            }

            if (elementoftable[5] != string.Empty)
            {
                oldVodka.Volume = int.Parse(elementoftable[5]);
            }

            if (elementoftable[6] != string.Empty)
            {
                oldVodka.Flavoured = elementoftable[6];
            }

            if (elementoftable[7] != string.Empty)
            {
                oldVodka.Material = elementoftable[7];
            }

            if (elementoftable[8] != string.Empty)
            {
                oldVodka.Qualification = elementoftable[8];
            }
        }

        private void Save()
        {
            this.entities.SaveChanges();
        }
    }
}
