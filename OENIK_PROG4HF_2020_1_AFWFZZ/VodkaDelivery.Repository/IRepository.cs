﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;

    /// <summary>
    /// Interface for Reposirories.
    /// </summary>
    /// <typeparam name="T">Can use any type of classes of Data.</typeparam>
    public interface IRepository<T>
    {
        T GetOne(int id);

        /// <summary>
        /// Visszadja az összes elemet.
        /// </summary>
        /// <returns>T típusú osztály.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Insert into the given table.
        /// </summary>
        /// <param name="newElement">This is the given table.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        bool InsertIn(T newElement);

        /// <summary>
        /// Update one element.
        /// </summary>
        /// /// <param name="id">Search through ids.</param>
        /// <param name="elementoftable">New details.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        bool Update(int id, List<string> elementoftable);

        /// <summary>
        /// Delete one element.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>True if could be deleted, false if could not.</returns>
        bool Delete(int id);

        /// <summary>
        /// Check if element with id is in T generic table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>true if yes.</returns>
        bool IsIn(int id);
    }

    /// <summary>
    /// Interface for non-CRUD.
    /// </summary>
    public interface IVodkaRepository : IRepository<Vodka>
    {
        /// <summary>
        /// Get the vodka datas and its company is datas.
        /// </summary>
        /// <param name="name">Search through names.</param>
        /// <returns>Vodkas and companies datas if its found it, null if not.</returns>
        IQueryable<Vodka> GetCompanyAndVodkaDatas(string name);
    }

    /// <summary>
    /// Interface for non-CRUD.
    /// </summary>
    public interface ICompanyRepository : IRepository<Company>
    {
        /// <summary>
        /// Return expensive vodkas.
        /// </summary>
        /// <returns>IQueryable of vodkas.</returns>
        IQueryable<Company> GetExpensiveVodkas();
    }

    /// <summary>
    /// Interface for non-CRUD.
    /// </summary>
    public interface IDeliveryRepository : IRepository<Delivery>
    {
        /// <summary>
        /// List of vodka's name and when were deliveried them.
        /// </summary>
        /// <param name="name">Name of vodka.</param>
        /// <returns>Vodka's name and.</returns>
        IQueryable<Delivery> GetDeliveriesUsingVodkaName(string name);
    }

    /// <summary>
    /// Interface for non-CRUD.
    /// </summary>
    public interface IShopRepository : IRepository<Shop>
    {
    }
}
