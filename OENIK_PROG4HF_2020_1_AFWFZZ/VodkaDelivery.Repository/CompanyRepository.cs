﻿// <copyright file="CompanyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VodkaDelivery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VodkaDelivery.Data;

    /// <summary>
    /// Company repository.
    /// </summary>
    public class CompanyRepository : ICompanyRepository
    {
        private readonly vodkadeliverydatabaseEntities entities = new vodkadeliverydatabaseEntities();

        /// <inheritdoc/>
        public Company GetOne(int id)
        {
            return this.entities.Company.SingleOrDefault(x => x.Company_ID == id);
        }

        /// <summary>
        /// Get all Companies.
        /// </summary>
        /// <returns>company.</returns>
        public IQueryable<Company> GetAll()
        {
            return this.entities.Company.Select(x => x).AsQueryable<Company>();
        }

        /// <summary>
        /// Insert into Company table.
        /// </summary>
        /// <param name="newElement">One element.</param>
        /// <returns>Return true, if element could insterted into table.</returns>
        public bool InsertIn(Company newElement)
        {
            this.entities.Company.Add(newElement);
            this.Save();
            return true;
        }

        /// <summary>
        /// Update one element from company table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <param name="elementoftable">New details.</param>
        /// <returns>True if could be updated, false if could not.</returns>
        public bool Update(int id, List<string> elementoftable)
        {
            if (this.IsIn(id))
            {
                Company oldCompany = this.entities.Company.Single(x => x.Company_ID == id);
                this.CompanyforUpdate(oldCompany, elementoftable);
                this.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Delete one element from company table.
        /// </summary>
        /// <param name="id">Search through ids.</param>
        /// <returns>True if could be deleted, false if could not.</returns>
        public bool Delete(int id)
        {
            if (id <= this.entities.Company.Count())
            {
                Company toDelete = this.entities.Company.Single(x => x.Company_ID == id);
                this.entities.Company.Remove(toDelete);
                this.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Return expensive vodkas.
        /// </summary>
        /// <returns>IQueryable of vodkas.</returns>
        public IQueryable<Company> GetExpensiveVodkas()
        {
            VodkaRepository repoJump = new VodkaRepository(this.entities);
            List<Company> companies = this.GetAll().ToList();
            List<Vodka> vodkas = repoJump.GetAll().ToList();

            var q = from company in companies
                    join vodka in vodkas on company.Company_ID equals vodka.Company_ID
                    where vodka.Price >= 6500
                    orderby company.Name descending
                    select company;

            return q.AsQueryable();
        }

        /// <summary>
        /// Check if table has element with that ID.
        /// </summary>
        /// <param name="id">Input ID.</param>
        /// <returns>True if tha ID is valid, False if not.</returns>
        public bool IsIn(int id)
        {
            if (id <= this.entities.Company.Count())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CompanyforUpdate(Company oldCompany, List<string> elementoftable)
        {
            if (elementoftable[0] != string.Empty)
            {
                oldCompany.Company_ID = int.Parse(elementoftable[0]);
            }

            if (elementoftable[1] != string.Empty)
            {
                oldCompany.Name = elementoftable[1];
            }

            if (elementoftable[2] != string.Empty)
            {
                oldCompany.Country = elementoftable[2];
            }

            if (elementoftable[3] != string.Empty)
            {
                oldCompany.Accessibility = elementoftable[3];
            }

            if (elementoftable[4] != string.Empty)
            {
                oldCompany.Establisment = int.Parse(elementoftable[4]);
            }

            if (elementoftable[5] != string.Empty)
            {
                oldCompany.CEO = elementoftable[5];
            }
        }

        private void Save()
        {
            this.entities.SaveChanges();
        }
    }
}
